package middle

import (
	"net/http"
	"runtime"

	"kalimasi/rsrc"
	"kalimasi/rsrc/log"
	"kalimasi/util"

	"github.com/google/uuid"
)

type DBMiddle string

func (am DBMiddle) GetName() string {
	return string(am)
}

func (am DBMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// one time scope setup area for middleware
		return func(w http.ResponseWriter, r *http.Request) {
			reqID := uuid.New()
			key := reqID.String()
			r = util.SetConnKey(r, key)
			f(w, r)
			di := rsrc.GetDI()
			di.Close(key)
			runtime.GC()
		}
	}
}

func getLog() *log.Logger {
	di := rsrc.GetDI()
	return di.GetLog()
}

// func getRedisByReq(req *http.Request) *redis.Client {
// 	di := rsrc.GetDI()
// 	return di.GetRedisByReq(req)
// }
