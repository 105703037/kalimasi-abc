package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"kalimasi/middle"
	"kalimasi/rsrc"
	userV1 "kalimasi/service/api/user/v1"
	userV2 "kalimasi/service/api/user/v2"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/rs/cors"
)

var (
	confpath = flag.String("confpath", "./", "write memory profile to `file`")
	v        = flag.Bool("v", false, "version")

	Version   = "1.0.0"
	BuildTime = "2000-01-01T00:00:00+0800"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		log.Printf("Defaulting to port %s", port)
	}

	// [START setting_port]
	env := os.Getenv("ENV")
	if env == "" {
		env = "dev"
		log.Printf("Defaulting to ENV %s", env)
	}

	timezone := os.Getenv("TIMEZONE")
	if timezone == "" {
		timezone = "Asia/Taipei"
		log.Printf("Defaulting to timezone %s", timezone)
	}

	rsrc.IniConfByEnv(env, timezone)

	di := rsrc.GetDI()
	router := mux.NewRouter()
	authMiddle := middle.AuthMiddle("auth")
	di.APIConf.InitAPI(
		router,
		[]rsrc.Middle{
			middle.DebugMiddle("debug"),
			middle.DBMiddle("db"),
			authMiddle,
		},
		authMiddle,
		userV1.BudgetAPI("budgetV1"),
		userV1.AccTermAPI("accTermV1"),
		userV1.ReceiptAPI("receiptV1"),
		userV1.TransferAPI("transferV1"),
		userV1.RebateAPI("rebateV1"),
		userV1.AuthAPI("authV1"),
		userV1.ReportAPI("reportV1"),
		userV1.MemberAPI("memberV1"),
		userV1.CompanyAPI("companyV1"),
		userV1.SmartAPI("smartV1"),
		userV2.AuthAPI("authV2"),
	)

	di.GetLog().Info(fmt.Sprintf("start service formicidae with version %s and build time %s", Version, BuildTime))

	if di.APIConf.EnableCORS {
		c := cors.New(cors.Options{
			AllowedOrigins:   []string{"http://eaccount.numax.com.tw", "https://eaccount.numax.com.tw"},
			AllowCredentials: true,
			AllowedHeaders:   []string{"Auth-Token", "Content-Type"},
			// Enable Debugging for testing, consider disabling in production
			Debug: false,
		})
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), c.Handler(router)))
	} else {
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), router))
	}
}
