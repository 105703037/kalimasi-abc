package model

import (
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc/log"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type companyModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetCompanyModel(mgodb *mgo.Database) *companyModel {
	mongo := GetMgoDBModel(mgodb)
	return &companyModel{dbmodel: mongo, log: mongo.log}
}

func (cm *companyModel) FindCompany(companyID bson.ObjectId, typ string, code string) (*doc.Company, error) {

	c := &doc.Company{
		ID: companyID,
	}

	err := cm.dbmodel.FindByID(c)
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (cm *companyModel) CreateAccTerm(bi *input.CreateAccTermItem, u *input.ReqUser) error {

	r := &doc.AccTermList{
		Typ:       bi.Typ,
		CompanyID: u.CompanyID,
	}

	return cm.dbmodel.Save(r, u)
}

func (cm *companyModel) CreatePayMethod(pi *input.PayAccTermMapConf, u *input.ReqUser) error {
	bs := &doc.Company{ID: u.CompanyID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	/*從資料庫取得原始資料*/
	um := bs
	bs.AddPayMethod(pi.GetAccDoc())

	err = cm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

func (cm *companyModel) ModifyPayMethod(pi *input.PutPayAccTermMapConf, u *input.ReqUser) error {
	bs := &doc.Company{ID: pi.ID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	/*從資料庫取得原始資料*/
	um := bs
	err = bs.ChangePayMethod(pi.GetAccDoc())
	if err != nil {
		return err
	}

	err = cm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

func (cm *companyModel) DeletePayMethod(methodName string, u *input.ReqUser) error {
	bs := &doc.Company{ID: u.CompanyID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	/*從資料庫取得原始資料*/
	um := bs
	err = bs.DeletePayMethod(methodName)
	if err != nil {
		return err
	}

	err = cm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

func (cm *companyModel) CreateIncome(pi *input.PayAccTermMapConf, u *input.ReqUser) error {
	bs := &doc.Company{ID: u.CompanyID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}

	/*從資料庫取得原始資料*/
	um := bs
	bs.AddIncome(pi.GetAccDoc())

	err = cm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

func (cm *companyModel) ModifyIncome(pi *input.PutPayAccTermMapConf, u *input.ReqUser) error {
	bs := &doc.Company{ID: pi.ID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}

	/*從資料庫取得原始資料*/
	um := bs
	err = bs.ChangeIncome(pi.GetAccDoc())
	if err != nil {
		return err
	}

	err = cm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}
func (cm *companyModel) DeleteIncome(incomeName string, u *input.ReqUser) error {
	bs := &doc.Company{ID: u.CompanyID}
	err := cm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	/*從資料庫取得原始資料*/
	um := bs
	err = bs.DeleteIncome(incomeName)
	if err != nil {
		return err
	}

	err = cm.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return cm.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}
