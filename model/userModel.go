package model

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc"
	"kalimasi/rsrc/log"
	"kalimasi/util"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type userModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetUserModel(mgodb *mgo.Database) *userModel {
	mongo := GetMgoDBModel(mgodb)
	return &userModel{dbmodel: mongo, log: mongo.log}
}

func (bm *userModel) Signin(l *input.Login) (token string, perm string, err error) {
	cp := util.MD5(l.Pwd)
	q := bson.M{"email": l.Account, "pwd": cp}
	u := &doc.User{}
	err = bm.dbmodel.PipelineOne(u, u.GetPipeline(q).GetPipeline())
	if err != nil {
		return "", "", err
	}
	if !u.ID.Valid() {
		return "", "", errors.New("not found")
	}

	ucp, err := u.GetUserCompanyPerm(l.Company, l.IsFakeCompany)
	if err != nil {
		return "", "", err
	}
	t, err := rsrc.GetDI().GetJWTConf().GetToken(
		map[string]interface{}{
			"sub": u.ID.Hex(),
			"acc": u.Email,
			"nam": u.DisplayName,
			"per": ucp.Permission,
			"cat": ucp.CompanyID.Hex(),
		})
	if err != nil {
		return "", "", err
	}
	return *t, ucp.Permission, nil
}

func (bm *userModel) SigninV2(l *input.LoginV2) (token string, err error) {
	cp := util.MD5(l.Pwd)
	q := bson.M{"email": l.Account, "pwd": cp}
	u := &doc.User{}
	err = bm.dbmodel.PipelineOne(u, u.GetPipeline(q).GetPipeline())
	if err != nil {
		return "", err
	}
	if !u.ID.Valid() {
		return "", errors.New("not found")
	}

	t, err := rsrc.GetDI().GetJWTConf().GetToken(
		map[string]interface{}{
			"sub": u.ID.Hex(),
			"nam": u.DisplayName,
			"acc": u.Email,
		})
	if err != nil {
		return "", err
	}
	return *t, nil
}
