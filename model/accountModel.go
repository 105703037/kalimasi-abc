package model

import (
	"kalimasi/doc"
	"kalimasi/rsrc/log"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// AccountModel is used to handle 會計帳目功能
type AccountModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

// GetAccountModel return an accountModel which passes mongodb parameters
// to generate MgoDbModel.
func GetAccountModel(mgodb *mgo.Database) *AccountModel {
	mongo := GetMgoDBModel(mgodb)
	return &AccountModel{dbmodel: mongo, log: mongo.log}
}

func (am *AccountModel) GetFinalAccount(companyID bson.ObjectId, year int) (*FinalAccountMap, error) {
	a := &doc.GroupAccount{
		Account: doc.Account{
			CompanyID: companyID,
		},
	}
	q := a.GetFinalPipeline(bson.M{"year": year})
	i, err := am.dbmodel.PipelineAll(a, q.GetPipeline())
	if err != nil {
		return nil, err
	}
	fam := &FinalAccountMap{}
	if result, ok := i.([]*doc.GroupAccount); ok {
		for _, r := range result {
			fam.Add(r.ID.Code, r.Name, r.ID.Typ, r.Amount)
		}
	}
	return fam, nil
}

type FinalAccountMap struct {
	m map[string]*doc.BudgetItem
}

// IsNil returns whether is struct is Nil.
func (fa *FinalAccountMap) IsNil() bool {
	return fa == nil
}

// GetBalaceSheetItem 帶入參數code(會計科目), 回傳預算項目.
func (fa *FinalAccountMap) GetBalaceSheetItem(code string) *doc.BudgetItem {
	if i, ok := fa.m[code]; ok {
		return &doc.BudgetItem{
			Code:   i.Code,
			Amount: i.Amount,
			Name:   i.Name,
			Desc:   i.Desc,
		}
	}
	return &doc.BudgetItem{}
}

func (fa *FinalAccountMap) GetBudgetItem(code string, typ string) *doc.BudgetItem {
	if i, ok := fa.m[code]; ok {
		if typ == doc.TypeAccIncome {
			return &doc.BudgetItem{
				Code:   i.Code,
				Amount: i.Amount * -1,
				Name:   i.Name,
				Desc:   i.Desc,
			}
		}
		return &doc.BudgetItem{
			Code:   i.Code,
			Amount: i.Amount,
			Name:   i.Name,
			Desc:   i.Desc,
		}
	}
	return &doc.BudgetItem{}
}

func (fa *FinalAccountMap) Add(code, name, typ string, amount int) {
	if fa.m == nil {
		fa.m = make(map[string]*doc.BudgetItem)
	}
	if typ == doc.TypeAccountCredit {
		amount = -1 * amount
	}

	if bb, ok := fa.m[code]; ok {
		bb.Amount += amount
	} else {
		fa.m[code] = &doc.BudgetItem{
			Code:   code,
			Name:   name,
			Amount: amount,
		}
	}
}
