package reportgener

import (
	"encoding/json"
	"errors"
	"fmt"
	"kalimasi/doc"
	"kalimasi/report"
	"kalimasi/rsrc"
	"kalimasi/util"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"
)

type balanceSheetDataInter interface {
	GetBalaceSheetItem(code string) *doc.BudgetItem
}

type BalanceSheetItem struct {
	Name       string
	AccCode    string
	AccTerm    string
	SpecialSub []*BalanceSheetItem
}

func (bsi *BalanceSheetItem) toReportColumn(data balanceSheetDataInter) *report.BalanceSheetItem {
	if bsi.SpecialSub != nil && len(bsi.SpecialSub) == 2 {
		item1 := data.GetBalaceSheetItem(bsi.SpecialSub[0].AccCode)
		item2 := data.GetBalaceSheetItem(bsi.SpecialSub[1].AccCode)
		return &report.BalanceSheetItem{
			Name1:   bsi.SpecialSub[0].Name,
			Amount1: item1.Amount,
			Name2:   bsi.SpecialSub[1].Name,
			Amount2: item2.Amount,
		}
	}

	item1 := data.GetBalaceSheetItem(bsi.AccCode)
	return &report.BalanceSheetItem{
		Name1:   bsi.Name,
		Amount1: item1.Amount,
	}
}

type SettingBalanceSheet struct {
	Name    string
	Sub     []*BalanceSheetItem
	AccCode string
	AccTerm string
}

func (sbs *SettingBalanceSheet) getItem(bs balanceSheetDataInter) []*report.BalanceSheetItem {
	first := &report.BalanceSheetItem{
		Name1:   sbs.Name,
		Amount1: 0,
		IsHead1: true,
	}
	var result []*report.BalanceSheetItem
	result = append(result, first)
	if len(sbs.Sub) == 0 {
		return result
	}
	for _, s := range sbs.Sub {
		b := s.toReportColumn(bs)
		result = append(result, b)
		first.Amount1 += (b.Amount1 - b.Amount2)
	}
	return result
}

type BalanceSheet struct {
	Title     string        `json:"-"`
	CompanyID bson.ObjectId `json:"-"`
	Year      int           `json:"-"`
	data      balanceSheetDataInter
	Assets    []*SettingBalanceSheet
	Liab      []*SettingBalanceSheet
}

func getBalanceSheetItemAry(sbsAry []*SettingBalanceSheet, bs balanceSheetDataInter) []*report.BalanceSheetItem {
	var r []*report.BalanceSheetItem
	for _, sbs := range sbsAry {
		parent := sbs.getItem(bs)
		r = append(r, parent...)
	}
	return r
}

func (bbr *BalanceSheet) getAssetItems(bs balanceSheetDataInter) []*report.BalanceSheetItem {
	return getBalanceSheetItemAry(bbr.Assets, bs)
}

func (bbr *BalanceSheet) getLiabItems(bs balanceSheetDataInter) []*report.BalanceSheetItem {
	return getBalanceSheetItemAry(bbr.Liab, bs)
}

func (bbr *BalanceSheet) GetReportID() bson.ObjectId {
	if !bbr.CompanyID.Valid() {
		panic("must set companyID")
	}
	return doc.GetReportID(bbr.GetType(), bbr.CompanyID)
}

func (bbr *BalanceSheet) GetFileName() string {
	if !bbr.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(bbr.CompanyID.Hex(), "/", strconv.Itoa(bbr.Year), "-balanceSheet-", strconv.FormatInt(time.Now().Unix(), 16))
}

func (bbr *BalanceSheet) IsSetting() bool {
	return bbr.Assets != nil && bbr.Liab != nil
}

func (bbr *BalanceSheet) GetData() string {
	data, _ := json.Marshal(bbr)
	return string(data)
}

func (bbr *BalanceSheet) GetTitle() string {
	return bbr.Title
}

func (bbr *BalanceSheet) GetType() string {
	return "balanceSheet"
}

func (bbr *BalanceSheet) Setting(setting string) error {
	err := json.Unmarshal([]byte(setting), bbr)
	if err != nil {
		return err
	}
	return nil
}

func (bbr *BalanceSheet) AddAssetsSetting(setting *SettingBalanceSheet) {
	bbr.Assets = append(bbr.Assets, setting)
}

func (bbr *BalanceSheet) AddLiabSetting(setting *SettingBalanceSheet) {
	bbr.Liab = append(bbr.Liab, setting)
}

func (bbr *BalanceSheet) SetData(bs balanceSheetDataInter) {
	bbr.data = bs
}

func (bbr *BalanceSheet) GetReportObj(title string, font map[string]string) (ReportInter, error) {
	if bbr.data == nil {
		return nil, errors.New("data not set")
	}
	di := rsrc.GetDI()
	year := bbr.Year + 1911
	end := time.Date(year, time.Month(12), 31, 0, 0, 0, 0, di.Location)
	style := report.BalanceBudgetStyle("default")
	result := report.BalanceSheet{
		Title:    title,
		DateTime: end,
		Style:    &style,
		Font:     font,
	}
	assetItems := bbr.getAssetItems(bbr.data)
	liabItems := bbr.getLiabItems(bbr.data)
	var rows []*report.BalanceSheetRow
	liabIndex, maxLiabIndex := 0, len(liabItems)-1
	for _, ai := range assetItems {
		if ai.Len() == 0 {
			continue
		}
		bsr := &report.BalanceSheetRow{}
		bsr.Asset = ai
		if ai.Len() == 1 && liabIndex <= maxLiabIndex {
			bsr.Liab = liabItems[liabIndex]
			liabIndex++
		} else if ai.Len() == 2 && liabIndex+1 <= maxLiabIndex {
			liabItems[liabIndex].Name2 = liabItems[liabIndex+1].Name1
			liabItems[liabIndex].Amount2 = liabItems[liabIndex+1].Amount1
			liabItems[liabIndex].IsHead2 = liabItems[liabIndex+1].IsHead1
			bsr.Liab = liabItems[liabIndex]
			liabIndex += 2
		} else if ai.Len() == 2 && liabIndex == maxLiabIndex {
			bsr.Liab = liabItems[liabIndex]
			liabIndex++
		} else {
			bsr.Liab = &report.BalanceSheetItem{}
		}
		rows = append(rows, bsr)
	}
	for liabIndex <= maxLiabIndex {
		bsr := &report.BalanceSheetRow{}
		bsr.Asset = &report.BalanceSheetItem{}
		bsr.Liab = liabItems[liabIndex]
		rows = append(rows, bsr)
		liabIndex++
	}
	result.Item = rows
	return &result, nil
}

func (bbr *BalanceSheet) SetEmpty() {
	if bbr.Assets != nil {
		for _, p := range bbr.Assets {
			if p.Sub == nil {
				p.Sub = []*BalanceSheetItem{}
			} else {
				for _, p2 := range p.Sub {
					fmt.Println(p2)
					if p2.SpecialSub == nil {
						p2.SpecialSub = []*BalanceSheetItem{}
					} else {
						p2.setEmpty()
					}
				}

			}
		}
	} else {
		bbr.Assets = []*SettingBalanceSheet{}
	}

	if bbr.Liab != nil {
		for _, p := range bbr.Liab {
			if p.Sub == nil {
				p.Sub = []*BalanceSheetItem{}
			} else {
				for _, p2 := range p.Sub {

					if p2.SpecialSub == nil {
						p2.SpecialSub = []*BalanceSheetItem{}

					} else {
						p2.setEmpty()
					}
				}

			}
		}
	} else {
		bbr.Liab = []*SettingBalanceSheet{}
	}
}

func (bbr *BalanceSheetItem) setEmpty() {

	for _, p := range bbr.SpecialSub {
		if p.SpecialSub == nil {
			p.SpecialSub = []*BalanceSheetItem{}
		} else {
			p.setEmpty()
		}
	}
}

func (bbr *BalanceSheetItem) Validate() (err error) {
	for _, p := range bbr.SpecialSub {

		if p.Name == "" {
			err = errors.New("invalid name")

			return err
		}
		if p.SpecialSub != nil {
			err = p.Validate()
		}
		if err != nil {
			return err
		}
	}
	return err
}
