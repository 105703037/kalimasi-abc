package model

import (
	"errors"
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc/log"
	"kalimasi/util"
	"strings"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type accTermModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetAccTermModel(mgodb *mgo.Database) *accTermModel {
	mongo := GetMgoDBModel(mgodb)
	return &accTermModel{dbmodel: mongo, log: mongo.log}
}

func (am *accTermModel) FindAccTerm(companyID bson.ObjectId, typ string, code string) (*doc.AccTerm, error) {
	id, err := doc.GetAccTermId(companyID, typ)
	if err != nil {
		return nil, err
	}
	at := &doc.AccTermList{
		ID: id,
	}
	err = am.dbmodel.FindByID(at)
	if err != nil {
		return nil, err
	}
	a := at.GetAccTerm(code)
	if a == nil {
		return nil, errors.New("accterm not found: " + code)
	}
	return a, nil
}

func (am *accTermModel) CreateAccTerm(bi *input.CreateAccTermItem, u *input.ReqUser) error {

	r := &doc.AccTermList{
		Typ:       bi.Typ,
		CompanyID: u.CompanyID,
	}

	return am.dbmodel.Save(r, u)
}

func (am *accTermModel) Modify(pi *input.PutAccTermItem, u *input.ReqUser) error {
	bs := &doc.AccTermList{ID: pi.ID, CompanyID: u.GetCompany()}
	err := am.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	/*從前端取得更新資料*/
	um := pi.GetAccTermItemDoc()

	um.ID = bs.ID
	um.CompanyID = u.GetCompany()
	/*AccTermList GetUpdateField update 2個欄位("terms","enable")*/
	err = am.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return am.dbmodel.addDocLog(bs, u, doc.ActUpdate)

}

/*只更新Enable，但前端輸入源也只帶入Enable，所以查詢ID時，把資料也記錄下來*/
func (am *accTermModel) ModifyCatagory(pi *input.PutAccTermCatagory, u *input.ReqUser) error {
	bs := &doc.AccTermList{ID: pi.ID, CompanyID: u.GetCompany()}
	err := am.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	/*從資料庫端取得更新資料*/
	um := bs
	um.ID = bs.ID
	um.CompanyID = u.GetCompany()
	um.Enable = pi.Enable

	err = am.dbmodel.Update(um, u)
	if err != nil {
		return err
	}
	return am.dbmodel.addDocLog(bs, u, doc.ActUpdate)
}

type accTermRef struct {
	RefReport           []string
	HasCompanyPayMethod bool
	HasCompanyIncome    bool
}

// 查詢有用到會科的地方
func (am accTermModel) Refrence(company bson.ObjectId, code string) *accTermRef {
	result := &accTermRef{}
	// 檢查Company Setting
	c := &doc.Company{
		ID: company,
	}
	err := am.dbmodel.FindByID(c)
	fmt.Println(err)
	if err != nil {
		result.HasCompanyPayMethod = false
		result.HasCompanyIncome = false
	} else {
		for _, p := range c.PayMethod {
			if p.AccTermCode == code {
				result.HasCompanyPayMethod = true
				break
			}
		}
		for _, i := range c.Incomes {
			if i.AccTermCode == code {
				result.HasCompanyIncome = true
				break
			}
		}
	}

	// 檢查Report Setting
	allReportTyp := doc.GetAllReportType()
	report := &doc.Report{}
	const colon = "\""
	checkCode := util.StrAppend(colon, code, colon)
	for _, k := range allReportTyp {
		report.ID = doc.GetReportID(k, company)
		err = am.dbmodel.FindByID(report)
		if err != nil {
			continue
		}
		if strings.Contains(report.JsonSetting, checkCode) {
			result.RefReport = append(result.RefReport, k)
		}
	}
	if len(result.RefReport) == 0 &&
		!result.HasCompanyPayMethod &&
		!result.HasCompanyIncome {
		return nil
	}
	return result
}
