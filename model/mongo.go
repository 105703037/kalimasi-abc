package model

import (
	"errors"
	"fmt"
	"net/http"
	"reflect"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"

	"kalimasi/doc"
	"kalimasi/rsrc"
	"kalimasi/rsrc/log"
	"kalimasi/util"
)

type mgoDBModel struct {
	db  *mgo.Database
	log *log.Logger

	indexExistMap map[string]bool
}

func GetMgoDBModel(c *mgo.Database) *mgoDBModel {
	di := rsrc.GetDI()
	return &mgoDBModel{
		db:            c,
		log:           di.GetLog(),
		indexExistMap: make(map[string]bool),
	}
}

func GetMgoDBModelByReq(req *http.Request) *mgoDBModel {
	di := rsrc.GetDI()
	db := di.GetMongoByReq(req)
	return GetMgoDBModel(db)
}

const (
	TxnC = "txn"
)

func (mm *mgoDBModel) RunTxn(ops []txn.Op) error {
	r := txn.NewRunner(mm.db.C(TxnC))
	id := bson.NewObjectId()
	err := r.Run(ops, id, nil)
	if err != nil {
		r.Resume(id)
		return err
	}
	return nil
}

func (mm *mgoDBModel) hasCreatedIndex(d doc.DocInter) bool {
	exist, ok := mm.indexExistMap[d.GetC()]
	if !ok {
		return false
	}
	return exist
}

func (mm *mgoDBModel) createIndex(collection *mgo.Collection, d doc.DocInter) error {
	for _, index := range d.GetMongoIndexes() {
		// Check if index already exists, if it does, skip
		if err := collection.EnsureIndex(index); err == nil {
			mm.log.Info(fmt.Sprintf("%s index already exist, skipping create step", index.Key[0]))
			continue
		}

		// Create index (keep in mind EnsureIndex is blocking operation)
		mm.log.Info(fmt.Sprintf("Creating %s index", index.Key[0]))
		if err := collection.DropIndex(index.Key[0]); err != nil {
			return err
		}
		if err := collection.EnsureIndex(index); err != nil {
			return err
		}
	}
	mm.indexExistMap[d.GetC()] = true
	return nil
}

func (mm *mgoDBModel) Save(d doc.DocInter, u doc.LogUser) error {
	collection := mm.db.C(d.GetC())
	if !mm.hasCreatedIndex(d) {
		mm.createIndex(collection, d)
	}

	d.SetCreator(u)
	d.SetCompany(u.GetCompany())
	err := collection.Insert(d.GetDoc())
	if err != nil {
		mm.log.Warn(err.Error())
		return err
	}
	return nil
}

func (mm *mgoDBModel) addDocLog(d doc.DocInter, u doc.LogUser, act string) error {
	log := doc.GetDocLog(d, u, act)
	collection := mm.db.C(log.GetC())
	if !mm.hasCreatedIndex(log) {
		mm.createIndex(collection, log)
	}
	err := collection.Insert(log.GetDoc())
	if err != nil {
		mm.log.Err(fmt.Sprintf("can not insert doc log: %s", err.Error()))
	}
	return nil
}

func (mm *mgoDBModel) Update(d doc.DocInter, u doc.LogUser) error {
	uf := d.GetUpdateField()
	if uf == nil {
		return errors.New("cant not update " + d.GetC())
	}
	err := mm.db.C(d.GetC()).UpdateId(d.GetID(), bson.M{"$set": d.GetUpdateField()})
	if err != nil {
		return err
	}
	return mm.addDocLog(d, u, doc.ActUpdate)
}

func (mm *mgoDBModel) RemoveByID(d doc.DocInter) error {
	return mm.db.C(d.GetC()).Remove(bson.M{"_id": d.GetID()})
}

func (mm *mgoDBModel) RemoveAll(d doc.DocInter, q bson.M) (int, error) {
	info, err := mm.db.C(d.GetC()).RemoveAll(q)
	return info.Removed, err
}

func (mm *mgoDBModel) FindByID(d doc.DocInter) error {
	if !d.GetID().Valid() {
		return errors.New("invalid id")
	}
	return mm.db.C(d.GetC()).FindId(d.GetID()).One(d)
}

func (mm *mgoDBModel) FindOne(d doc.DocInter, q bson.M) error {
	return mm.db.C(d.GetC()).Find(q).One(d)
}

func (mm *mgoDBModel) Count(d doc.DocInter, q bson.M) int {
	n, err := mm.db.C(d.GetC()).Find(q).Count()
	if err != nil {
		mm.log.Err(err.Error())
	}
	return n
}

func (mm *mgoDBModel) Find(d doc.DocInter, q bson.M, limit, page int) (interface{}, error) {
	myType := reflect.TypeOf(d)
	slice := reflect.MakeSlice(reflect.SliceOf(myType), 0, 0).Interface()
	mgoQ := mm.db.C(d.GetC()).Find(q)
	if limit > 0 {
		mgoQ = mgoQ.Limit(limit)
	}
	if page > 0 {
		mgoQ = mgoQ.Skip(limit * (page - 1))
	}
	err := mgoQ.All(&slice)
	return slice, err
}

func (mm *mgoDBModel) PipelineOne(d doc.DocInter, p []bson.M) error {
	pipe := mm.db.C(d.GetC()).Pipe(p)
	return pipe.One(d)
}

func (mm *mgoDBModel) PipelineAll(d doc.DocInter, p []bson.M) (interface{}, error) {
	myType := reflect.TypeOf(d)
	slice := reflect.MakeSlice(reflect.SliceOf(myType), 0, 0).Interface()
	pipe := mm.db.C(d.GetC()).Pipe(p)
	err := pipe.All(&slice)
	return slice, err
}

func (mm *mgoDBModel) Iter(d doc.DocInter, q bson.M) *mgo.Iter {
	return mm.db.C(d.GetC()).Find(q).Iter()
}

const (
	MaxLimit = 200
)

func (mm *mgoDBModel) Paginator(d doc.DocInter, q bson.M, limit, page int,
	format func(i interface{}) map[string]interface{}) (*util.PaginationResV2, error) {
	mgoQuery := mm.db.C(d.GetC()).Find(q)
	paginator, err := util.MongoPaginationV2(mgoQuery, limit, page)
	if err != nil {
		return nil, err
	}
	result, err := mm.Find(d, q, limit, page)
	if err != nil {
		return nil, err
	}
	if format != nil {
		d, _ := doc.Format(result, format)
		paginator.Rows = d
	} else {
		paginator.Rows = result
	}
	return paginator, nil
}

func (mm *mgoDBModel) PaginatorPipeline(d doc.DocInter, pq doc.PipelineQryInter, limit, page int,
	format func(i interface{}) map[string]interface{}) (*util.PaginationResV2, error) {
	mgoQuery := mm.db.C(d.GetC()).Find(pq.GetQ())
	paginator, err := util.MongoPaginationV2(mgoQuery, limit, page)
	if err != nil {
		return nil, err
	}
	pl := pq.GetPipeline()
	pl = append(pl,
		bson.M{
			"$skip": paginator.Limit * (paginator.Page - 1),
		},
		bson.M{
			"$limit": paginator.Limit,
		})
	result, err := mm.PipelineAll(d, pl)
	if err != nil {
		return nil, err
	}
	if format != nil {
		d, _ := doc.Format(result, format)
		paginator.Rows = d
	} else {
		paginator.Rows = result
	}
	return paginator, nil
}
