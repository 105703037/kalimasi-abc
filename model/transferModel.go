package model

import (
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc/log"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

type transferModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetTransferModel(mgodb *mgo.Database) *transferModel {
	mongo := GetMgoDBModel(mgodb)
	return &transferModel{dbmodel: mongo, log: mongo.log}
}

func (bm *transferModel) Create(bi *input.CreateTransfer, u *input.ReqUser) error {
	bs := bi.GetTransferDoc()
	var ops []txn.Op

	for _, d := range bi.DebitAccTerm {
		acc := d.GetAccount(bi.Date, doc.TypeAccountDebit, bi.BudgetId, u.CompanyID)
		ops = append(ops, acc.GetSaveTxnOp(u))
		bs.DebitAccTerm = append(bs.DebitAccTerm, acc.GetMiniAccount())
	}

	for _, c := range bi.CreditAccTerm {
		acc := c.GetAccount(bi.Date, doc.TypeAccountCredit, bi.BudgetId, u.CompanyID)
		ops = append(ops, acc.GetSaveTxnOp(u))
		bs.CreditAccTerm = append(bs.CreditAccTerm, acc.GetMiniAccount())
	}

	ops = append(ops, bs.GetSaveTxnOp(u))

	return bm.dbmodel.RunTxn(ops)
}

func (bm *transferModel) Modify(pi *input.PutTransfer, u *input.ReqUser) error {
	or := &doc.Transfer{ID: pi.ID, CompanyID: u.CompanyID}
	err := bm.dbmodel.FindByID(or)
	if err != nil {
		return err
	}
	var ops []txn.Op

	delAcc := &doc.Account{CompanyID: u.CompanyID}
	for _, d := range pi.DeleteAccterm {
		delAcc.ID = d
		ops = append(ops, delAcc.GetDelTxnOp())
	}

	or.DebitAccTerm = or.DebitAccTerm[:0]
	or.CreditAccTerm = or.CreditAccTerm[:0]

	for _, d := range pi.DebitAccTerm {
		acc := d.GetAccount(pi.Date, doc.TypeAccountDebit, pi.BudgetId, or.CompanyID)
		if acc.ID.Valid() {
			ops = append(ops, acc.GetUpdateTxnOp(
				bson.D{
					{Name: "code", Value: d.Code},
					{Name: "name", Value: d.Name},
					{Name: "amount", Value: d.Amount},
					{Name: "summary", Value: d.Desc},
					{Name: "datetime", Value: pi.Date},
					{Name: "year", Value: pi.Date.Year() - 1911},
					{Name: "budgetid", Value: pi.BudgetId},
				}))
		} else {
			ops = append(ops, acc.GetSaveTxnOp(u))
		}
		or.DebitAccTerm = append(or.DebitAccTerm, acc.GetMiniAccount())
	}

	for _, d := range pi.CreditAccTerm {
		acc := d.GetAccount(pi.Date, doc.TypeAccountDebit, pi.BudgetId, u.CompanyID)
		if acc.ID.Valid() {
			ops = append(ops, acc.GetUpdateTxnOp(
				bson.D{
					{Name: "code", Value: d.Code},
					{Name: "name", Value: d.Name},
					{Name: "amount", Value: d.Amount},
					{Name: "summary", Value: d.Desc},
					{Name: "datetime", Value: pi.Date},
					{Name: "year", Value: pi.Date.Year() - 1911},
					{Name: "budgetid", Value: pi.BudgetId},
				}))
		} else {
			ops = append(ops, acc.GetSaveTxnOp(u))
		}

		or.CreditAccTerm = append(or.CreditAccTerm, acc.GetMiniAccount())
	}

	ops = append(ops, or.GetUpdateTxnOp(
		bson.D{
			{Name: "datetime", Value: pi.Date},
			{Name: "year", Value: pi.Date.Year() - 1911},
			{Name: "summary", Value: pi.Summary},
			{Name: "budgetid", Value: pi.BudgetId},
			{Name: "debitaccterm", Value: or.DebitAccTerm},
			{Name: "creditaccterm", Value: or.CreditAccTerm},
		}))

	err = bm.dbmodel.RunTxn(ops)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(or, u, doc.ActUpdate)
}

func (bm *transferModel) Delete(pi doc.DocInter, u *input.ReqUser) error {
	or := &doc.Transfer{ID: pi.GetID(), CompanyID: u.CompanyID}
	err := bm.dbmodel.FindByID(or)
	if err != nil {
		return err
	}
	var ops []txn.Op

	for _, a := range or.CreditAccTerm {
		ops = append(ops, a.GetDelTxnOp(u.CompanyID))
	}

	for _, a := range or.DebitAccTerm {
		ops = append(ops, a.GetDelTxnOp(u.CompanyID))
	}

	ops = append(ops, or.GetDelTxnOp())

	err = bm.dbmodel.RunTxn(ops)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(or, u, doc.ActDelete)
}
