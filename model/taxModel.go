/*
 * 報稅相關
 */
package model

import (
	"kalimasi/rsrc/log"

	"github.com/globalsign/mgo"
)

type taxModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetTaxModel(mgodb *mgo.Database) *taxModel {
	mongo := GetMgoDBModel(mgodb)
	return &taxModel{
		dbmodel: mongo,
		log:     mongo.log,
	}
}
