package model

import (
	"kalimasi/rsrc/log"

	"github.com/globalsign/mgo"
	elastic "github.com/olivere/elastic/v7"
)

type smartModel struct {
	db     *mgo.Database
	search *elastic.Client

	log *log.Logger
}
