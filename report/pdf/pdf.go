package pdf

import (
	"io"
	"os"
	"strings"

	"github.com/94peter/gopdf"
)

type pdf struct {
	myPDF                                            *gopdf.GoPdf
	width, height                                    float64
	leftMargin, topMargin, rightMargin, bottomMargin float64
	page                                             uint8
}

func GetA4PDF(fontMap map[string]string, leftMargin, rightMargin, topMargin, bottomMargin float64) pdf {
	gpdf := gopdf.GoPdf{}
	width, height := 595.28, 841.89
	gpdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: width, H: height}}) //595.28, 841.89 = A4
	var err error
	for key, value := range fontMap {
		err = gpdf.AddTTFFont(key, value)
		if err != nil {
			panic(err)
		}
	}
	gpdf.SetLeftMargin(leftMargin)
	gpdf.SetTopMargin(topMargin)
	return pdf{
		myPDF:        &gpdf,
		width:        width,
		height:       height,
		leftMargin:   leftMargin,
		rightMargin:  rightMargin,
		topMargin:    topMargin,
		bottomMargin: bottomMargin,
	}
}

func GetA4HPDF(fontMap map[string]string, leftMargin, rightMargin, topMargin, bottomMargin float64) pdf {
	gpdf := gopdf.GoPdf{}
	height, width := 595.28, 841.89
	gpdf.Start(gopdf.Config{PageSize: gopdf.Rect{W: width, H: height}}) //595.28, 841.89 = A4
	var err error
	for key, value := range fontMap {
		err = gpdf.AddTTFFont(key, value)
		if err != nil {
			panic(err)
		}
	}
	gpdf.SetLeftMargin(leftMargin)
	gpdf.SetTopMargin(topMargin)
	return pdf{
		myPDF:        &gpdf,
		width:        width,
		height:       height,
		leftMargin:   leftMargin,
		rightMargin:  rightMargin,
		topMargin:    topMargin,
		bottomMargin: bottomMargin,
	}
}

const (
	ValignTop    = 1
	ValignMiddle = 2
	ValignBottom = 3
	AlignLeft    = 4
	AlignCenter  = 5
	AlignRight   = 6
)

func (p *pdf) WriteToFile(filepath string) error {
	pdf := p.myPDF
	return pdf.WritePdf(filepath)
}

func (p *pdf) Write(w io.Writer) error {
	pdf := p.myPDF
	return pdf.Write(w)
}

func (p *pdf) AddPage() {
	p.page++
	pdf := p.myPDF
	pdf.AddPage()
}

func (p *pdf) Br(h float64) {
	pdf := p.myPDF
	pdf.Br(h)
	pdf.SetX(p.leftMargin)
}

func (p *pdf) GetWidth() float64 {
	return p.width - p.leftMargin - p.rightMargin
}

func (p *pdf) GetBottomHeight() float64 {
	return p.height - p.bottomMargin
}

func (p *pdf) Line(width float64) {
	pdf := p.myPDF
	pdf.SetLineWidth(width)
	pdf.Line(p.leftMargin, pdf.GetY(), p.width-p.rightMargin, pdf.GetY())
}

func (p *pdf) TextWithPosition(text string, style TextStyle, x, y float64) {
	pdf := p.myPDF
	pdf.SetFont(style.Font, "", style.FontSize)
	textw, _ := pdf.MeasureTextWidth(text)
	rightLimit := p.width - p.rightMargin - textw
	if x < p.leftMargin {
		x = p.leftMargin
	} else if x > rightLimit {
		x = rightLimit
	}
	pdf.SetX(x)
	pdf.SetY(y)

	color := style.Color
	pdf.SetTextColor(color.R, color.G, color.B)
	pdf.SetFillColor(color.R, color.G, color.B)

	pdf.Cell(nil, text)
	pdf.SetX(x + textw)
}

func (p *pdf) Text(text string, style TextStyle, align int) {
	pdf := p.myPDF
	pdf.SetFont(style.Font, "", style.FontSize)
	color := style.Color
	pdf.SetTextColor(color.R, color.G, color.B)
	pdf.SetFillColor(color.R, color.G, color.B)
	ox := pdf.GetX()
	if ox < p.leftMargin {
		ox = p.leftMargin
	}
	x := ox
	textw, _ := pdf.MeasureTextWidth(text)
	switch align {
	case AlignCenter:
		x = (p.width / 2) - (textw / 2)
	case AlignRight:
		x = p.width - textw - p.rightMargin
	}
	pdf.SetX(x)
	pdf.Cell(nil, text)
	pdf.SetX(ox + textw)
}

func (p *pdf) TwoColumnText(text1, text2 string, style TextStyle) {
	pdf := p.myPDF
	pdf.SetFont(style.Font, "", style.FontSize)
	color := style.Color
	pdf.SetTextColor(color.R, color.G, color.B)
	pdf.SetX(p.leftMargin)
	pdf.Cell(nil, text1)
	pdf.SetX(p.width/2 + p.leftMargin)
	pdf.Cell(nil, text2)
}

func (p *pdf) ImageReader(imageByte io.Reader) {
	//use image holder by io.Reader
	imgH2, err := gopdf.ImageHolderByReader(imageByte)
	if err != nil {
		panic(err)
	}
	pdf := p.myPDF
	pdf.ImageByHolder(imgH2, p.leftMargin, pdf.GetY(), nil)
}

func (p *pdf) Image(imagePath string) {
	//use image holder by io.Reader
	file, err := os.Open(imagePath)
	if err != nil {
		panic(err)
	}
	imgH2, err := gopdf.ImageHolderByReader(file)
	if err != nil {
		panic(err)
	}
	pdf := p.myPDF
	pdf.ImageByHolder(imgH2, p.leftMargin, pdf.GetY(), nil)
}

func (p *pdf) RectDrawColor(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
) {
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "D")
}

func (p *pdf) RectDrawColorWithPosition(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
	x, y float64,
) {
	pdf := p.myPDF
	ox, oy := pdf.GetX(), pdf.GetY()
	pdf.SetX(x)
	pdf.SetY(y)
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "D")
	pdf.SetX(ox)
	pdf.SetY(oy)
}

func (p *pdf) RectFillDrawColor(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
) {
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "FD")
}

func (p *pdf) RectFillDrawColorWithPosition(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
	x, y float64,
) {
	pdf := p.myPDF
	ox, oy := pdf.GetX(), pdf.GetY()
	pdf.SetX(x)
	pdf.SetY(y)
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "FD")
	pdf.SetX(ox)
	pdf.SetY(oy)
}

func (p *pdf) RectFillColorWithPosition(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
	x, y float64,
) {
	pdf := p.myPDF
	ox, oy := pdf.GetX(), pdf.GetY()
	pdf.SetX(x)
	pdf.SetY(y)
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "F")
	pdf.SetX(ox)
	pdf.SetY(oy)
}

func (p *pdf) RectFillColor(text string,
	style TextBlockStyle,
	w, h float64,
	align, valign int,
) {
	p.rectColorText(text, style.Font, style.Style, style.FontSize, style.Color, w, h, style.BackGround, align, valign, "F")
}

func (p *pdf) rectColorText(text string,
	font string,
	style string,
	fontSize int,
	textColor Color,
	w, h float64,
	color Color,
	align, valign int,
	rectType string,
) {
	pdf := p.myPDF
	pdf.SetLineWidth(0.1)
	pdf.SetFont(font, style, fontSize)
	pdf.SetFillColor(color.R, color.G, color.B) //setup fill color
	ox, x := pdf.GetX(), 0.0

	if ox < p.leftMargin {
		ox = p.leftMargin
	}
	x = ox
	pdf.RectFromUpperLeftWithStyle(x, pdf.GetY(), w, h, rectType)
	pdf.SetFillColor(0, 0, 0)

	s := strings.Split(text, "\n")
	text = s[0]
	for _, t := range s {
		if len(t) > len(text) {
			text = t
		}
	}
	if align == AlignCenter {
		textw, _ := pdf.MeasureTextWidth(text)
		x = x + (w / 2) - (textw / 2)
	} else if align == AlignRight {
		textw, _ := pdf.MeasureTextWidth(text)
		x = x + w - textw
	} else {
		x = x + 5
	}

	pdf.SetX(x)
	oy, y := pdf.GetY(), 0.0
	if valign == ValignMiddle {
		y = oy + (h / 2) - (float64(fontSize) * float64(len(s)) / 2)
	} else if valign == ValignBottom {
		y = oy + h - float64(fontSize)*float64(len(s))
	}
	pdf.SetY(y)

	pdf.SetTextColor(textColor.R, textColor.G, textColor.B)

	i := 0.0
	for _, t := range s {
		// fmt.Println(t)
		pdf.SetY(y + float64(fontSize)*i)
		pdf.SetX(x)
		pdf.Cell(nil, t)
		i++
	}

	pdf.SetY(oy)
	pdf.SetX(ox + w)

}

func (p *pdf) GetX() float64 {
	return p.myPDF.GetX()
}

func (p *pdf) GetY() float64 {
	return p.myPDF.GetY()
}
