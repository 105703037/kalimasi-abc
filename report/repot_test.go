package report

import (
	"fmt"
	"kalimasi/doc"
	"kalimasi/rsrc"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_balanceBudget(t *testing.T) {
	rsrc.InitConfByFile("../conf/test/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	ds := BalanceBudgetStyle("default")
	now := time.Now()
	bb := BalanceBudget{
		Title:     "總會社團",
		StartDate: now.AddDate(-1, 0, 0),
		EndDate:   now,
		Style:     &ds,
		Font:      di.GetFontMap(),
	}
	bb.AddItem(&BalanceBudgetItem{
		Category:   "1",
		AccTerm:    "本會收入",
		Amount:     3072590,
		LastAmount: nil,
		Increase:   72590,
	})

	bb.AddItem(&BalanceBudgetItem{
		SubCategory: "1",
		AccTerm:     "入會費",
		Amount:      70000,
		LastAmount:  nil,
		Increase:    10000,
		Desc:        "一二三四五六七八九十一二三一二三四五",
	})

	f, _ := os.Create("./week_report.pdf")
	defer f.Close()
	err := bb.PDF(f)
	fmt.Println(err)
	assert.True(t, false)
}

func Test_balanceSheet(t *testing.T) {
	rsrc.InitConfByFile("../conf/test/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	ds := BalanceBudgetStyle("default")
	now := time.Now()
	bb := BalanceSheet{
		Title:    "總會社團",
		DateTime: now.AddDate(-1, 0, 0),

		Style: &ds,
		Font:  di.GetFontMap(),
	}

	bb.AddItem(&BalanceSheetRow{
		Asset: &BalanceSheetItem{
			Name1:   "房屋及建築",
			Amount1: 10000,
			Name2:   "累計折舊－房屋及建築",
			Amount2: 1000,
		},
		Liab: &BalanceSheetItem{
			Name1:   "基金",
			Amount1: 1000,
			Name2:   "基金",
			Amount2: 1000,
		},
	})

	bb.AddItem(&BalanceSheetRow{
		Asset: &BalanceSheetItem{
			Name1:   "房屋及建築",
			Amount1: 10000,
			Name2:   "累計折舊－房屋及建築",
			Amount2: 1000,
		},
		Liab: &BalanceSheetItem{
			Name1:   "基金",
			Amount1: 1000,
		},
	})

	bb.AddItem(&BalanceSheetRow{
		Asset: &BalanceSheetItem{
			Name1:   "房屋及建築",
			Amount1: 10000,
			Name2:   "累計折舊－房屋及建築",
			Amount2: 1000,
		},
		Liab: &BalanceSheetItem{
			Name1:   "基金",
			Amount1: 1000,
		},
	})

	f, _ := os.Create("./week_report.pdf")
	defer f.Close()
	err := bb.PDF(f)
	fmt.Println(err)
	assert.True(t, false)
}

func Test_r401Sheet(t *testing.T) {
	rsrc.InitConfByFile("../conf/test/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	ds := BalanceBudgetStyle("default")
	bb := SalesAndTax{
		Style: &ds,
		Font:  di.GetFontMap(),
	}

	f, _ := os.Create("./week_report.pdf")
	defer f.Close()
	err := bb.PDF(f)
	fmt.Println(err)
	assert.True(t, false)
}

func Test_taxCal(t *testing.T) {
	rsrc.InitConfByFile("../conf/test/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	ds := TaxCalStyle(true)
	bb := TaxCal{
		OrgName:           "關公交流協會",
		Year:              108,
		StartTime:         time.Now(),
		EndTime:           time.Now().AddDate(1, 0, 0),
		UnitCode:          "12345678",
		IsTaxFree:         false,
		OpenMonth:         12,
		OverseasTaxCredit: 0,
		TaxCredit:         1000,
		LostInvoice:       100000,

		Style: &ds,
		Font:  di.GetFontMap(),
	}

	bb.AddIncome(&doc.TaxItem{
		Term:         "1. 捐款收入",
		Desc:         "desc",
		Amount:       1000,
		AdjustAmount: 1000,
		Ps:           "test",
	})
	bb.AddIncome(&doc.TaxItem{
		Term:         "2. 會費收入",
		Desc:         "desc",
		Amount:       196000,
		AdjustAmount: 196000,
		Ps:           "test",
	})

	bb.AddExpenditure(&doc.TaxItem{
		Term:         "1. 薪資資出",
		Desc:         "desc",
		Amount:       502,
		AdjustAmount: 502,
		Ps:           "test",
	})
	bb.AddExpenditure(&doc.TaxItem{
		Term:         "2. 薪資資出",
		Desc:         "desc",
		Amount:       502,
		AdjustAmount: 502,
		Ps:           "test",
	})

	f, _ := os.Create("./week_report.pdf")
	defer f.Close()
	err := bb.PDF(f)
	fmt.Println(err)
	assert.True(t, false)
}

func Test_autoLine(t *testing.T) {
	m := "中文abcdefgh"
	nm, line := autoEnter(m, 5)
	fmt.Println(nm, line)
	assert.True(t, false)
}

func Test_strToByte(t *testing.T) {
	c := getReceiptFormatCode(&doc.Receipt{
		TypeAcc: doc.TypeAccIncome,
		Typ:     doc.TypeReceiptDuplicate,
	})
	fmt.Println(c)
	assert.True(t, false)
}

func Test_EnterIncome_TXT(t *testing.T) {
	ei := EnterIncome{
		taxID:     "123456789",
		vatNumber: "12345678",
	}
	ei.AddReceipt(&doc.Receipt{
		DateTime:  time.Now(),
		Typ:       doc.TypeReceiptTriplicate,
		TypeAcc:   doc.TypeAccIncome,
		Number:    "AB12345678",
		VATnumber: "87654321",
		Amount:    123,
		TaxInfo: doc.Tax{
			Typ:    doc.TypeTaxTable,
			Amount: 6,
		},
	})
	f, err := os.Create("dat2.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()
	err = ei.TXT(f)
	fmt.Println(err)
	assert.True(t, false)
}

func Test_SalesAndTax_TXT(t *testing.T) {
	sat := SalesAndTax{
		DataType: 1,
	}
	err := sat.SetVATnumber("12345678")
	if err != nil {
		fmt.Println(err)
	}
	f, err := os.Create("dat2.txt")
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()
	err = sat.TXT(f)
	fmt.Println(err)
	assert.True(t, false)
}

func TestJournal(t *testing.T) {
	rsrc.InitConfByFile("../conf/test/config.yml", "Asia/Taipei")
	di := rsrc.GetDI()
	ds := BalanceBudgetStyle("default")
	now := time.Now()
	bb := Journal{
		Title:  "總會社團",
		Start:  now.AddDate(-1, 0, 0),
		End:    now,
		Create: now,
		Style:  &ds,
		Font:   di.GetFontMap(),
	}

	item := JournalItem{
		Date: time.Now(),
		No:   "1111",
	}
	item.AddAcc(&JournalAcc{
		AccCode: "111",
		AccName: "dddd",
		Amount:  10000,
		Typ:     "debit",
	})

	item.AddAcc(&JournalAcc{
		AccCode: "311",
		AccName: "dddd",
		Amount:  10000,
		Typ:     "credit",
	})

	bb.AddItem(&item)

	f, _ := os.Create("./journal_report.pdf")
	defer f.Close()
	err := bb.PDF(f)
	fmt.Println(err)
	assert.True(t, false)
}
