/*
 * 401報表
 */
package report

import (
	"encoding/json"
	"fmt"
	"io"
	"kalimasi/doc"
	"kalimasi/report/pdf"
	"math"
	"time"
)

var (
	singleAlignCenter = []int{pdf.AlignCenter}
	singleAlignRight  = []int{pdf.AlignRight}
	singleAlignLeft   = []int{pdf.AlignLeft}
)

func getTableColumns(preTerm string, serial int, i *doc.TaxItem) []*pdf.TableColumn {
	newTerm, line := autoEnter(i.Term, 9)
	fl := float64(line)
	rowHeight := (fl-1)*0.5*20 + 20

	return []*pdf.TableColumn{
		pdf.GetTableColumn(20, rowHeight, singleAlignLeft, newTerm),
		pdf.GetTableColumn(20, rowHeight, singleAlignCenter, i.Desc),
		pdf.GetTableColumn(90, rowHeight, singleAlignCenter, fmt.Sprintf("%s%02d", preTerm, serial)),
		pdf.GetTableColumn(80, rowHeight, singleAlignRight, currencyFormat(&i.Amount)),
		pdf.GetTableColumn(80, rowHeight, singleAlignRight, currencyFormat(&i.AdjustAmount)),
		pdf.GetTableColumn(75, rowHeight, singleAlignRight, i.Ps),
	}
}

type TaxCal struct {
	Year      int       `json:"year"`
	OrgName   string    `json:"companyName"`
	StartTime time.Time `json:"startTime"`
	EndTime   time.Time `json:"endTime"`
	UnitCode  string    `json:"unitCode"` //統一編號

	Incomes     []*doc.TaxItem `json:"incomes"`
	Expenditure []*doc.TaxItem `json:"expenses"`

	IsTaxFree         bool `json:"isTaxFree"`         // 是否免稅
	TaxFreeAmount     int  `json:"taxFreeAmount"`     // 免稅額
	OpenMonth         int  `json:"openMonth"`         // 營業月數
	TaxCredit         int  `json:"taxCredit"`         // 抵稅額
	OverseasTaxCredit int  `json:"overseasTaxCredit"` // 境外抵稅額
	LostInvoice       int  `json:"lostInvoice"`       // 遺失憑證金額

	Style *TaxCalStyle      `json:"-"`
	Font  map[string]string `json:"-"`

	commonReport
}

func (bb *TaxCal) AddIncome(bbi *doc.TaxItem) {
	bb.Incomes = append(bb.Incomes, bbi)
}

func (bb *TaxCal) AddExpenditure(bbi *doc.TaxItem) {
	bb.Expenditure = append(bb.Expenditure, bbi)
}

func (bb *TaxCal) Json(w io.Writer) error {
	return json.NewEncoder(w).Encode(bb)
}

func (bb *TaxCal) PDF(w io.Writer) error {
	const title = "%d年度機關或團體及其作業組織餘挫及稅額計算表"
	p := pdf.GetA4PDF(bb.Font, 20, 20, 20, 20)
	p.AddPage()
	style := bb.Style
	// page one report
	p.Text(fmt.Sprintf(title, bb.Year), style.GetTitle(), pdf.AlignCenter)
	p.Br(25)
	p.Text(fmt.Sprintf("機關或團體名稱：%s", bb.OrgName), style.GetContent(), pdf.AlignLeft)
	p.Br(16)
	p.Text(fmt.Sprintf("所得期間：自民國 %d年 %d月 %d日起至 %d年 %d月 %d日止",
		bb.StartTime.Year()-1911, bb.StartTime.Month(), bb.StartTime.Day(),
		bb.EndTime.Year()-1911, bb.EndTime.Month(), bb.EndTime.Day()),
		style.GetContent(), pdf.AlignLeft)
	p.Br(20)
	p.RectFillDrawColorWithPosition("扣繳單位\n統一編號", style.TextBlock(10), 50, 25, pdf.AlignCenter, pdf.ValignMiddle, 340, 45)

	l := len(bb.UnitCode)
	for i := 0; i < l; i++ {
		p.RectFillDrawColorWithPosition(string(bb.UnitCode[i]), style.TextBlock(12), 20, 25, pdf.AlignCenter, pdf.ValignMiddle, float64(390+i*20), 45)
	}

	nt := pdf.GetNormalTable()
	ha := bb.getHeaderAry()
	for _, h := range ha {
		nt.AddHeader(h)
	}
	total, totalAjust := calTotal(bb.Incomes)

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "04 收入"),
		pdf.GetTableColumn(20, 20, singleAlignCenter, "(請加總填列)"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, "04"),
		pdf.GetTableColumn(80, 20, singleAlignRight, currencyFormat(&total)),
		pdf.GetTableColumn(80, 20, singleAlignRight, currencyFormat(&totalAjust)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})
	for i, v := range bb.Incomes {
		nt.AddRow(getTableColumns("04", i+1, v))
	}

	total06, totalAjust06 := calTotal(bb.Expenditure)

	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "06 支出"),
		pdf.GetTableColumn(20, 20, singleAlignCenter, "(請加總填列)"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, "06"),
		pdf.GetTableColumn(80, 20, singleAlignRight, currencyFormat(&total06)),
		pdf.GetTableColumn(80, 20, singleAlignRight, currencyFormat(&totalAjust06)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})
	for i, v := range bb.Expenditure {
		nt.AddRow(getTableColumns("06", i+1, v))
	}
	subTotal := total - total06
	subAdjustTotal := totalAjust - totalAjust06
	nt.AddRow([]*pdf.TableColumn{
		pdf.GetTableColumn(20, 20, singleAlignLeft, "09 本期餘絀數"),
		pdf.GetTableColumn(20, 20, singleAlignCenter, "(01 - 05)"),
		pdf.GetTableColumn(90, 20, singleAlignCenter, "09"),
		pdf.GetTableColumn(80, 20, singleAlignRight, currencyFormat(&subTotal)),
		pdf.GetTableColumn(80, 20, singleAlignRight, currencyFormat(&subAdjustTotal)),
		pdf.GetTableColumn(75, 20, singleAlignRight, ""),
	})
	nt.Draw(&p, style)
	if p.GetBottomHeight() < p.GetY()+100 {
		p.AddPage()
	}
	p.RectFillDrawColor("", style.TextBlock(10), 555, 100, pdf.AlignCenter, pdf.ValignMiddle)
	p.RectFillColorWithPosition("課稅所得額計算表", style.TextBlock(10), 200, 20, pdf.AlignCenter, pdf.ValignMiddle, 168, p.GetY()+2)
	p.Br(12)
	var checkFirst, checkSec string
	if bb.IsTaxFree {
		checkFirst = "■"
		checkSec = "□"
	} else {
		checkFirst = "□"
		checkSec = "■"
	}
	p.RectFillColorWithPosition(fmt.Sprintf("1. %s", checkFirst), style.TextBlock(10), 20, 20, pdf.AlignLeft, pdf.ValignMiddle, p.GetX()+5, p.GetY()+16)
	p.RectFillColorWithPosition("符合「教育文化公益慈善機關或團體免納所得稅適用標準」第2條、第4條規定，且無依該標準第3條規定應課徵\n所得稅之銷售貨務或勞務之所得，本期免納所得稅",
		style.TextBlock(10), 440, 20, pdf.AlignLeft, pdf.ValignMiddle, p.GetX()+30, p.GetY()+16)
	p.Br(24)
	p.RectFillColorWithPosition(fmt.Sprintf("2. %s", checkSec), style.TextBlock(10), 20, 20, pdf.AlignLeft, pdf.ValignMiddle, p.GetX()+5, p.GetY()+20)
	p.RectFillColorWithPosition(fmt.Sprintf("未符合前項免稅適用標準規定，本期應課徵所得稅"),
		style.TextBlock(10), 440, 20, pdf.AlignLeft, pdf.ValignMiddle, p.GetX()+30, p.GetY()+20)
	p.Br(16)
	trueIncome := subAdjustTotal - bb.TaxFreeAmount
	p.RectFillColorWithPosition(
		`17 課稅所得額 = 上表09　　　　　　　　元 - 24 依其他法令規定免稅所得　　　　　　　　元 = 17　　　　　　　　元`,
		style.TextBlock(9), 450, 20, pdf.AlignLeft, pdf.ValignMiddle, p.GetX()+5, p.GetY()+20)
	if bb.IsTaxFree {
		p.RectFillColorWithPosition(currencyFormat(nil), style.UTextBlock(11), 66, 16, pdf.AlignRight, pdf.ValignMiddle, p.GetX()+108, p.GetY()+22)
		p.RectFillColorWithPosition(currencyFormat(nil), style.UTextBlock(11), 66, 16, pdf.AlignRight, pdf.ValignMiddle, p.GetX()+308, p.GetY()+22)
		p.RectFillColorWithPosition(currencyFormat(nil), style.UTextBlock(11), 66, 16, pdf.AlignRight, pdf.ValignMiddle, p.GetX()+408, p.GetY()+22)
	} else {
		p.RectFillColorWithPosition(currencyFormat(&subAdjustTotal), style.UTextBlock(11), 66, 16, pdf.AlignRight, pdf.ValignMiddle, p.GetX()+108, p.GetY()+22)
		p.RectFillColorWithPosition(currencyFormat(&bb.TaxFreeAmount), style.UTextBlock(11), 66, 16, pdf.AlignRight, pdf.ValignMiddle, p.GetX()+308, p.GetY()+22)
		p.RectFillColorWithPosition(currencyFormat(&trueIncome), style.UTextBlock(11), 66, 16, pdf.AlignRight, pdf.ValignMiddle, p.GetX()+408, p.GetY()+22)
	}

	p.Br(48)

	if p.GetBottomHeight() < p.GetY()+145 {
		p.AddPage()
	}

	p.RectFillDrawColor("稅額\n計算", style.TextBlock(10), 25, 145, pdf.AlignCenter, pdf.ValignMiddle)
	p.RectFillDrawColor("", style.TextBlock(10), 440, 65, pdf.AlignCenter, pdf.ValignMiddle)
	p.RectFillColorWithPosition(
		`18 課稅所得額 x 稅率 = 本年度應納稅額(計算至元為止，角以下無條件捨去)
(申報及計算稅額請先閱第2頁背面申報須知)`,
		style.TextBlock(9), 400, 20, pdf.AlignLeft, pdf.ValignMiddle, 46, p.GetY()+5)
	p.RectFillColorWithPosition(
		`(1) 【　　　　　　　　元　Ｘ　　　％】＝`,
		style.TextBlock(9), 400, 20, pdf.AlignLeft, pdf.ValignMiddle, 46, p.GetY()+25)
	p.RectFillColorWithPosition(
		`(2) 營業期間不滿1年者，換算全年所得核課：【(　　　　　　　　ｘ ━ ) Ｘ　　 ％】X ━ ＝`,
		style.TextBlock(9), 400, 20, pdf.AlignLeft, pdf.ValignMiddle, 46, p.GetY()+40)
	twelve := 12
	p.RectFillColorWithPosition(currencyFormat(&twelve), style.TextBlock(9), 11, 11, pdf.AlignCenter, pdf.ValignMiddle, 326, p.GetY()+38)
	p.RectFillColorWithPosition(currencyFormat(&twelve), style.TextBlock(9), 11, 11, pdf.AlignCenter, pdf.ValignMiddle, 397, p.GetY()+51)

	p.RectFillDrawColor("18　　　　　　元", style.TextBlock(10), 90, 65, pdf.AlignCenter, pdf.ValignMiddle)

	taxRate, tax := getTaxRate(trueIncome), 0
	if taxRate == 19 && trueIncome <= 193548 {
		tax = int(math.Floor(float64(trueIncome-120000)/2.0 + 0.5))
	}

	if bb.OpenMonth == twelve {
		p.RectFillColorWithPosition(currencyFormat(&trueIncome), style.UTextBlock(11), 66, 16, pdf.AlignRight, pdf.ValignMiddle, 76, p.GetY()+27)
		p.RectFillColorWithPosition(currencyFormat(&taxRate), style.UTextBlock(11), 20, 16, pdf.AlignCenter, pdf.ValignMiddle, 175, p.GetY()+27)
		if tax == 0 {
			tax = int(math.Floor(float64(trueIncome)*float64(taxRate)/100 + 0.5))
		}
		p.RectFillColorWithPosition(currencyFormat(&tax), style.UTextBlock(11), 55, 16, pdf.AlignRight, pdf.ValignMiddle, 230, p.GetY()+27)
	} else if bb.OpenMonth > 0 {
		p.RectFillColorWithPosition(currencyFormat(&trueIncome), style.UTextBlock(11), 66, 16, pdf.AlignRight, pdf.ValignMiddle, 248, p.GetY()+42)
		p.RectFillColorWithPosition(currencyFormat(&taxRate), style.UTextBlock(11), 20, 16, pdf.AlignCenter, pdf.ValignMiddle, 352, p.GetY()+42)
		p.RectFillColorWithPosition(currencyFormat(&bb.OpenMonth), style.UTextBlock(9), 11, 11, pdf.AlignCenter, pdf.ValignMiddle, 326, p.GetY()+52)
		p.RectFillColorWithPosition(currencyFormat(&bb.OpenMonth), style.UTextBlock(9), 11, 11, pdf.AlignCenter, pdf.ValignMiddle, 397, p.GetY()+37)
		if tax == 0 {
			tax = int(math.Floor(float64(trueIncome)*float64(twelve)/float64(bb.OpenMonth)*float64(taxRate)/100*float64(bb.OpenMonth)/float64(twelve) + 0.5))
		}
		p.RectFillColorWithPosition(currencyFormat(&tax), style.UTextBlock(11), 55, 16, pdf.AlignRight, pdf.ValignMiddle, 420, p.GetY()+42)
	}
	p.RectFillColorWithPosition(currencyFormat(&tax), style.UTextBlock(11), 55, 16, pdf.AlignRight, pdf.ValignMiddle, 505, p.GetY()+25)

	p.RectFillDrawColorWithPosition("33 依境外所得來源國稅法規定繳納之所得稅可扣抵稅額(附所得稅法第3條第2項規定之納稅憑證)", style.TextBlock(10), 440, 20, pdf.AlignLeft, pdf.ValignMiddle, 45, p.GetY()+65)
	p.RectFillDrawColorWithPosition("33　　　　　　元", style.TextBlock(10), 90, 20, pdf.AlignCenter, pdf.ValignMiddle, 485, p.GetY()+65)
	p.RectFillColorWithPosition(currencyFormat(&bb.OverseasTaxCredit), style.UTextBlock(11), 55, 16, pdf.AlignRight, pdf.ValignMiddle, 505, p.GetY()+67)

	p.RectFillDrawColorWithPosition("21 本年度抵繳之扣繳稅額", style.TextBlock(10), 440, 20, pdf.AlignLeft, pdf.ValignMiddle, 45, p.GetY()+85)
	p.RectFillDrawColorWithPosition("21　　　　　　元", style.TextBlock(10), 90, 20, pdf.AlignCenter, pdf.ValignMiddle, 485, p.GetY()+85)
	p.RectFillColorWithPosition(currencyFormat(&bb.TaxCredit), style.UTextBlock(11), 55, 16, pdf.AlignRight, pdf.ValignMiddle, 505, p.GetY()+87)

	moreTax := tax - bb.OverseasTaxCredit - bb.TaxCredit
	if moreTax < 0 {
		moreTax = 0
	}
	p.RectFillDrawColorWithPosition("22 本年度應自行向公庫補繳之稅額", style.TextBlock(10), 440, 20, pdf.AlignLeft, pdf.ValignMiddle, 45, p.GetY()+105)
	p.RectFillDrawColorWithPosition("22　　　　　　元", style.TextBlock(10), 90, 20, pdf.AlignCenter, pdf.ValignMiddle, 485, p.GetY()+105)
	p.RectFillColorWithPosition(currencyFormat(&moreTax), style.UTextBlock(11), 55, 16, pdf.AlignRight, pdf.ValignMiddle, 505, p.GetY()+107)

	taxSubOverseasTaxCredit := tax - bb.OverseasTaxCredit
	if taxSubOverseasTaxCredit < 0 {
		taxSubOverseasTaxCredit = 0
	}
	returnTax := bb.TaxCredit + moreTax - taxSubOverseasTaxCredit
	p.RectFillDrawColorWithPosition("23 本年度申請應退還之稅額(21+22)-(18-33)、(18-33) < 0 以0計", style.TextBlock(10), 440, 20, pdf.AlignLeft, pdf.ValignMiddle, 45, p.GetY()+125)
	p.RectFillDrawColorWithPosition("23　　　　　　元", style.TextBlock(10), 90, 20, pdf.AlignCenter, pdf.ValignMiddle, 485, p.GetY()+125)
	p.RectFillColorWithPosition(currencyFormat(&returnTax), style.UTextBlock(11), 55, 16, pdf.AlignRight, pdf.ValignMiddle, 505, p.GetY()+127)

	p.Br(145)

	if p.GetBottomHeight() < p.GetY()+45 {
		p.AddPage()
	}

	p.RectFillDrawColor("揭露\n事項", style.TextBlock(10), 25, 45, pdf.AlignCenter, pdf.ValignMiddle)
	p.RectFillDrawColor("本機關或團體本期費用及損失，因交易相對人應給與而未給與統一發票，致無法取得合法憑證，惟已誠實入帳，\n \n能提示送貨單、交易相關文件及支付款項資料者，計　　　　　　元。", style.TextBlock(10), 530, 45, pdf.AlignLeft, pdf.ValignMiddle)
	p.RectFillColorWithPosition(currencyFormat(&bb.LostInvoice), style.UTextBlock(11), 50, 16, pdf.AlignRight, pdf.ValignMiddle, 285, p.GetY()+25)

	p.Br(45)

	p.RectFillDrawColor("附註", style.TextBlock(10), 25, 75, pdf.AlignCenter, pdf.ValignMiddle)
	p.RectFillDrawColor(
		`一、本表各欄如不敷應用請依式自製附件使用。
二、獲配之股利或盈餘(含股票股利)，應於收入項目欄內填列，並依免稅適用標準規定徵、免所得稅。
三、主管機關為提昇社會福利機構之服務品質或為鼓勵業者配合辦理相關業務所給與獎助性質之各項補助類，
　　如無須相對提供勞務或服務者，非屬「銷售貨物或勞務」收入。
四、承辦政府委辦業務所取得之收入，及接受政府機關安置收托或收容身心障礙者，所領取之托育及養護補助費收入
　　屬銷售貨物或勞務收入，請填報申報書第４頁及第５頁。`,
		style.TextBlock(10), 530, 75, pdf.AlignLeft, pdf.ValignMiddle)

	p.Br(85)
	p.Text("簽證會計師：　　　　　　　(蓋章)", style.GetContent(), pdf.AlignLeft)
	p.Br(20)
	p.RectFillDrawColorWithPosition("分　　局\n稽  徵  所\n收件編號", style.TextBlock(10), 45, 40, pdf.AlignCenter, pdf.ValignMiddle, p.GetX()+360, p.GetY())
	p.RectFillDrawColorWithPosition("", style.TextBlock(10), 150, 40, pdf.AlignCenter, pdf.ValignMiddle, p.GetX()+405, p.GetY())
	return p.Write(w)
}

func getTaxRate(taxableIncome int) int {
	if taxableIncome <= 120000 {
		return 0
	}
	if taxableIncome <= 500000 {
		return 19
	}
	return 20
}

func calTotal(items []*doc.TaxItem) (total, adjustTotal int) {
	for _, i := range items {
		total += i.Amount
		adjustTotal += i.AdjustAmount
	}
	return
}

func (bb *TaxCal) getHeaderAry() []pdf.TableHeaderColumn {
	var ha []pdf.TableHeaderColumn
	ha = append(ha,
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"項目"},
				Width:  110,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"摘要"},
				Width:  100,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"帳載結算金額"},
				Width:  125,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"自行依法調整後金額"},
				Width:  120,
				Height: 20,
			}},
		pdf.TableHeaderColumn{
			Main: pdf.TableColumn{
				Text:   []string{"備註"},
				Width:  100,
				Height: 20,
			}},
	)
	ha[0].AddSub("", 110, 0)
	ha[1].AddSub("", 100, 0)
	ha[2].AddSub("", 25, 0)
	ha[2].AddSub("", 100, 0)

	ha[3].AddSub("", 120, 0)
	ha[4].AddSub("", 100, 0)

	return ha
}

type TaxCalStyle bool

func (ds *TaxCalStyle) GetTitle() pdf.TextStyle {
	return pdf.TextStyle{
		Font:     "tw-m",
		FontSize: 16,
		Color:    pdf.ColorBlack,
	}
}

func (ds *TaxCalStyle) GetSubTitle() pdf.TextStyle {
	return pdf.TextStyle{
		Font:     "tw-r",
		FontSize: 12,
		Color:    pdf.ColorBlack,
	}
}

func (ds *TaxCalStyle) GetContent() pdf.TextStyle {
	return pdf.TextStyle{
		Font:     "tw-r",
		FontSize: 12,
		Color:    pdf.ColorBlack,
	}
}

func (ds *TaxCalStyle) Header() pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-m",
			FontSize: 12,
			Color:    pdf.ColorBlack,
		},
		BackGround: pdf.ColorWhite,
	}
}

func (ds *TaxCalStyle) Data() pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-r",
			FontSize: 10,
			Color:    pdf.ColorBlack,
		},
		W:          30,
		H:          10.0,
		BackGround: pdf.ColorWhite,
		TextAlign:  "left",
	}
}

func (ds *TaxCalStyle) TextBlock(size int) pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-m",
			FontSize: size,
			Color:    pdf.ColorBlack,
		},
		W:          30,
		H:          10.0,
		BackGround: pdf.ColorWhite,
		TextAlign:  "left",
	}
}

func (ds *TaxCalStyle) UTextBlock(size int) pdf.TextBlockStyle {
	return pdf.TextBlockStyle{
		TextStyle: pdf.TextStyle{
			Font:     "tw-m",
			FontSize: size,
			Style:    "",
			Color:    pdf.ColorBlack,
		},
		W:          30,
		H:          10.0,
		BackGround: pdf.ColorGray,
		TextAlign:  "left",
	}
}
