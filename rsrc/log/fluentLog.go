package log

const (
	LogTargetFluent = "fluent"
)

type fluentLog struct {
	Host []string
	Port int
}
