package log

import (
	"io"
	"log"
	"os"
)

const (
	LogTargetOS = "os"
)

type Logger struct {
	Level  string `yaml:"level"`
	Target string `yaml:"target"`
}

var (
	_logging *log.Logger
)

const (
	InfoPrefix  = "INFO "
	DebugPrefix = "DEBUG "
	ErrorPrefix = "ERROR "
	WarnPrefix  = "WARN "
	FatalPrefix = "FATAL "

	debugLevel = 1
	infoLevel  = 2
	warnLevel  = 3
	errorLevel = 4
	fatalLevel = 5
)

var (
	levelMap = map[string]int{
		"info":  infoLevel,
		"debug": debugLevel,
		"warn":  warnLevel,
		"error": errorLevel,
		"fatal": fatalLevel,
	}

	myLevel = 0
)

func (l Logger) GetLogging() *log.Logger {
	return _logging
}

func (l Logger) StartLog() {
	level, ok := levelMap[l.Level]
	if !ok {
		level = 0
	}
	myLevel = level

	var out io.Writer

	switch l.Target {
	default:
		out = os.Stdout
	}
	_logging = log.New(out, InfoPrefix, log.Ldate|log.Lmicroseconds|log.Llongfile)
}

func (l Logger) Info(msg string) {
	if myLevel > infoLevel {
		return
	}
	_logging.SetPrefix(InfoPrefix)
	_logging.Output(2, msg)
}

func (l Logger) Debug(msg string) {
	if myLevel > debugLevel {
		return
	}
	_logging.SetPrefix(DebugPrefix)
	_logging.Output(2, msg)
}

func (l Logger) Warn(msg string) {
	if myLevel > warnLevel {
		return
	}
	_logging.SetPrefix(WarnPrefix)
	_logging.Output(2, msg)
}

func (l Logger) Err(msg string) {
	if myLevel > errorLevel {
		return
	}
	_logging.SetPrefix(ErrorPrefix)
	_logging.Output(2, msg)
}

func (l Logger) Fatal(msg string) {
	if myLevel > fatalLevel {
		return
	}
	_logging.SetPrefix(FatalPrefix)
	_logging.Output(2, msg)
}
