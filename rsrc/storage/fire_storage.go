package storage

import (
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"log"
	"strings"

	gs "cloud.google.com/go/storage"
	firebase "firebase.google.com/go"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

type myFirebase struct {
	Bucket          string
	CredentialsFile string
}

func (fb *myFirebase) getBucket() (*gs.BucketHandle, error) {
	config := &firebase.Config{
		StorageBucket: fb.Bucket,
	}
	opt := option.WithCredentialsFile(fb.CredentialsFile)
	app, err := firebase.NewApp(context.Background(), config, opt)
	if err != nil {
		return nil, err
	}

	client, err := app.Storage(context.Background())
	if err != nil {
		return nil, err
	}

	bucket, err := client.DefaultBucket()
	if err != nil {
		return nil, err
	}
	return bucket, nil
}

func (fb *myFirebase) Save(fp string, file []byte) (string, error) {
	return fb.SaveByReader(fp, bytes.NewReader(file))
}

func (fb *myFirebase) SaveByReader(fp string, reader io.Reader) (string, error) {
	bucket, err := fb.getBucket()
	if err != nil {
		return "", err
	}
	ctx := context.Background()
	wc := bucket.Object(fp).NewWriter(ctx)

	if _, err := io.Copy(wc, reader); err != nil {
		return "", err
	}

	if err := wc.Close(); err != nil {
		return "", err
	}

	acl := bucket.Object(fp).ACL()
	if err := acl.Set(ctx, gs.AllUsers, gs.RoleReader); err != nil {
		return "", err
	}

	objectAttrsToUpdate := gs.ObjectAttrsToUpdate{
		CacheControl: "max-age=0",
	}
	attrs, err := bucket.Object(fp).Update(ctx, objectAttrsToUpdate)
	return attrs.MediaLink, nil
}

func (fb *myFirebase) Delete(object string) error {
	bucket, err := fb.getBucket()
	if err != nil {
		return err
	}
	ctx := context.Background()
	o := bucket.Object(object)
	if err = o.Delete(ctx); err != nil {
		return err
	}
	return nil
}

func (fb *myFirebase) Get(ob string) ([]byte, error) {
	bucket, err := fb.getBucket()
	if err != nil {
		return nil, err
	}
	ctx := context.Background()
	rc, err := bucket.Object(ob).NewReader(ctx)
	if err != nil {
		return nil, err
	}
	defer rc.Close()
	data, err := ioutil.ReadAll(rc)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (fb *myFirebase) FileExist(ob string) bool {
	bucket, err := fb.getBucket()
	if err != nil {
		log.Println(err.Error())
		return false
	}
	ctx := context.Background()
	o := bucket.Object(ob)
	attrs, err := o.Attrs(ctx)
	if err != nil {
		return false
	}
	return attrs.Size > 0
}

func (fb *myFirebase) List(dir string) []string {
	bucket, err := fb.getBucket()
	if err != nil {
		return nil
	}
	ctx := context.Background()
	it := bucket.Objects(ctx, &gs.Query{
		Prefix:    dir,
		Delimiter: "/",
	})
	var result []string
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if attrs.Prefix != "" {
			result = append(result, attrs.Prefix)
		}
		if attrs.Name != "" && !strings.HasSuffix(attrs.Name, "/") {
			result = append(result, attrs.Name)
		}
	}
	return result
}
