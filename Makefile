VERSION=`git describe --tags`
BUILD_TIME=`date +%FT%T%z`
LDFLAGS=-ldflags "-X main.Version=${VERSION} -X main.BuildTime=${BUILD_TIME}"

run: clear
	go build ${LDFLAGS} -o ./bin/api ./main.go
	./bin/api 

build-win-install: clear-win
	GOOS=windows GOARCH=386 go build -o ./exe/kalimasi.exe ./main.go

gcp:
	gcloud config set project kalimasi
	gcloud app deploy

clear:
	rm -rf ./bin/$(SERVICE)

clear-win:
	rm -rf ./build/exe/*