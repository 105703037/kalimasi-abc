package input

import (
	"errors"
	"kalimasi/doc"

	"github.com/globalsign/mgo/bson"
)

type PayAccTermMapConf struct {
	Display     string //付款方式
	AccTermCode string //會計科目
	Enable      bool
}

type PutPayAccTermMapConf struct {
	Display     string //付款方式
	AccTermCode string //會計科目
	Name        string
	Enable      bool
	ID          bson.ObjectId
}

func (pm *PayAccTermMapConf) Validate() error {
	if pm.AccTermCode == "" {
		return errors.New("invalid accTermCode")
	}

	if pm.Display == "" {
		return errors.New("invalid display")
	}

	return nil
}
func (pm *PutPayAccTermMapConf) Validate() error {
	if pm.AccTermCode == "" {
		return errors.New("invalid accTermCode")
	}

	if pm.Name == "" {
		return errors.New("invalid name")
	}

	if pm.Display == "" {
		return errors.New("invalid display")
	}

	return nil
}

func (pm *PayAccTermMapConf) GetAccDoc() *doc.AccMapping {
	m := &doc.AccMapping{
		Enable:      pm.Enable,
		AccTermCode: pm.AccTermCode,
		Display:     pm.Display,
	}

	return m
}

func (pm *PutPayAccTermMapConf) GetAccDoc() *doc.AccMapping {
	m := &doc.AccMapping{
		Enable:      pm.Enable,
		Name:        pm.Name,
		AccTermCode: pm.AccTermCode,
		Display:     pm.Display,
	}

	return m
}
