package input

import (
	"errors"
	"time"

	"kalimasi/doc"
	"kalimasi/util"

	"github.com/asaskevich/govalidator"
	"github.com/globalsign/mgo/bson"
)

type CreateReceipt struct {
	BudgetId        *bson.ObjectId `json:"budgetId"`
	AccType         string         `json:"accType"`
	Typ             string         `json:"type"`
	Date            time.Time      `json:"rDate"`
	Number          string
	VATnumber       string `json:"VATnumber"` // 統一編號
	Amount          int
	IsCollect       bool     `json:"isCollect"`
	CollectQuantity int      `json:"collectQuantity"`
	IsFixedAsset    bool     `json:"isFixedAsset"`
	Pictures        []string `json:"pictures"`

	TaxInfo Tax `json:"tax"`

	DebitAccTerm  []*InputAccTerm `json:"debitAccTermList"`
	CreditAccTerm []*InputAccTerm `json:"creditAccTermList"`
}

type Tax struct {
	Typ    string `json:"type"`
	Amount int
}

func (cu *Tax) Validate() error {
	if !util.IsStrInList(cu.Typ, allowTaxTyp...) {
		return errors.New("invalid tax type")
	}

	if (cu.Typ == doc.TypeTaxZero || cu.Typ == doc.TypeTaxNone) && cu.Amount > 0 {
		return errors.New("amount error")
	}
	return nil
}

var (
	allowAccType = []string{
		doc.TypeAccExpand,
		doc.TypeAccIncome,
	}
	allowReceiptTyp = []string{
		doc.TypeReceiptElectronic,
		doc.TypeReceiptTriplicate,
		doc.TypeReceiptDuplicate,
		doc.TypeReceiptInvoice,
		doc.TypeReceiptCustoms,
	}

	allowTaxTyp = []string{
		doc.TypeTaxTable,
		doc.TypeTaxFree,
		doc.TypeTaxZero,
		doc.TypeTaxNone,
	}
)

func (bi *CreateReceipt) GetReceiptDoc() *doc.Receipt {
	r := &doc.Receipt{
		DateTime:        bi.Date,
		Year:            bi.Date.Year() - 1911,
		Number:          bi.Number,
		VATnumber:       bi.VATnumber,
		Typ:             bi.Typ,
		TypeAcc:         bi.AccType,
		Amount:          bi.Amount,
		BudgetId:        bi.BudgetId,
		IsCollect:       bi.IsCollect,
		CollectQuantity: bi.CollectQuantity,
		IsFixedAsset:    bi.IsFixedAsset,
		TaxInfo: doc.Tax{
			Typ:    bi.TaxInfo.Typ,
			Amount: bi.TaxInfo.Amount,
		},
	}
	return r
}

func (cu *CreateReceipt) Validate() error {
	if !util.IsStrInList(cu.AccType, allowAccType...) {
		return errors.New("invalid account type")
	}
	if !util.IsStrInList(cu.Typ, allowReceiptTyp...) {
		return errors.New("invalid receipt type")
	}
	if !govalidator.IsNumeric(cu.VATnumber) {
		return errors.New("invalid VATnumber")
	}
	if cu.Amount < 0 {
		return errors.New("amount can not be negative")
	}
	if len(cu.CreditAccTerm) == 0 {
		return errors.New("creditAccTerm length can not be empty")
	}

	if len(cu.DebitAccTerm) == 0 {
		return errors.New("creditAccTerm length can not be empty")
	}

	if cu.IsCollect && cu.CollectQuantity <= 0 {
		return errors.New("collectQuantity must greater than 0")
	}

	if err := cu.TaxInfo.Validate(); err != nil {
		return err
	}

	creditTotal, debitTotal := 0, 0

	for _, s := range cu.CreditAccTerm {
		if err := s.Validate(); err != nil {
			return err
		}
		creditTotal += s.Amount
	}

	for _, s := range cu.DebitAccTerm {
		if err := s.Validate(); err != nil {
			return err
		}
		debitTotal += s.Amount
	}

	if creditTotal != debitTotal {
		return errors.New("credit and debit amount not equal")
	}

	if cu.Amount != creditTotal {
		return errors.New("receit amount not equal account term amount")
	}

	return nil
}

type PutReceipt struct {
	CreateReceipt
	ID            bson.ObjectId
	DelCreditList []bson.ObjectId `json:"delCreditList"`
	DelDebitList  []bson.ObjectId `json:"delDebitList"`
	DelPictures   []string        `json:"delPictures"`
}

func (pb *PutReceipt) Validate() error {
	err := pb.CreateReceipt.Validate()
	if err != nil {
		return err
	}
	if !pb.ID.Valid() {
		return errors.New("invalid receipt id")
	}
	return nil
}

type QueryReceipt struct {
	Number    string
	Typ       string
	AccType   string
	CompanyID bson.ObjectId

	BudgetID  string
	StartDate string
	EndDate   string
}

func (qb *QueryReceipt) Validate() error {
	if !qb.CompanyID.Valid() {
		return errors.New("invalid companyID")
	}
	if qb.Typ != "" && !util.IsStrInList(qb.Typ, allowReceiptTyp...) {
		return errors.New("invalid receipt type")
	}

	if qb.AccType != "" && !util.IsStrInList(qb.AccType, allowAccType...) {
		return errors.New("invalid account type")
	}

	if qb.BudgetID != "" && !bson.IsObjectIdHex(qb.BudgetID) {
		return errors.New("invalid budget")
	}

	st, err1 := time.Parse(time.RFC3339, qb.StartDate)
	ed, err2 := time.Parse(time.RFC3339, qb.EndDate)
	if err1 == nil && err2 == nil && st.Unix() > ed.Unix() {
		return errors.New("endDate must bigger than startDate")
	}
	return nil
}

func (qb *QueryReceipt) GetMgoQuery() bson.M {
	q := bson.M{
		"companyid": qb.CompanyID,
	}
	if qb.Number != "" {
		q["number"] = bson.RegEx{Pattern: qb.Number, Options: "m"}
	}
	if qb.Typ != "" {
		q["typ"] = qb.Typ
	}
	if qb.AccType != "" {
		q["typeacc"] = qb.AccType
	}
	if qb.BudgetID != "" {
		oid, err := doc.GetObjectID(qb.BudgetID)
		if err == nil {
			q["budgetid"] = oid
		}
	}
	st, err1 := time.Parse(time.RFC3339, qb.StartDate)
	ed, err2 := time.Parse(time.RFC3339, qb.EndDate)
	if err1 == nil && err2 == nil && st.Unix() <= ed.Unix() {
		q["datetime"] = bson.M{"$gte": st, "$lte": ed}
	}
	return q
}
