package input

import (
	"errors"
	"fmt"
	"kalimasi/doc"
	"strings"
	"time"

	"github.com/globalsign/mgo/bson"
)

type CreateAccTermItem struct {
	Typ   string
	Terms []*accTermGroup
}

type PutAccTermItem struct {
	CreateAccTermItem
	Enable bool
	ID     bson.ObjectId
}

type PutAccTermCatagory struct {
	Enable bool
	ID     bson.ObjectId
}

type accTermGroup struct {
	Enable bool
	Name   string
	Code   string
	Desc   string
	Sub    []*subAccTerm
}

type subAccTerm struct {
	Enable bool
	Name   string
	Code   string
	Desc   string
}

type InputAccTerm struct {
	Id     *bson.ObjectId
	Name   string
	Code   string
	Amount int
	Desc   string
}

func (cb *CreateAccTermItem) Validate() error {

	if doc.TypeMapCode[cb.Typ] <= 0 || doc.TypeMapCode[cb.Typ] >= 7 {
		return errors.New("invalid accterm type")
	}
	return nil
}

func (at *InputAccTerm) Validate() error {
	if at.Name == "" {
		return errors.New("invalid accterm name")
	}
	if at.Code == "" {
		return errors.New("invalid accterm code")
	}
	return nil
}

func (at *InputAccTerm) GetAccount(dt time.Time, typ string, budgetID *bson.ObjectId, companyID bson.ObjectId) *doc.Account {
	acc := &doc.Account{
		DateTime:  dt,
		Year:      dt.Year() - 1911,
		Typ:       typ,
		CompanyID: companyID,
		BudgetID:  budgetID,
		Summary:   at.Desc,
		Code:      at.Code,
		Name:      at.Name,
		Amount:    at.Amount,
	}
	if at.Id != nil {
		acc.ID = *(at.Id)
	}
	return acc
}

type QueryAccTerm struct {
	Typ       string
	CompanyId bson.ObjectId
}

func (qb *QueryAccTerm) Validate() error {
	return nil
}

func (ib *CreateAccTermItem) ToDoc() *doc.AccTermList {
	at := &doc.AccTermList{
		Typ: ib.Typ,
	}
	return at
}

func (qb *QueryAccTerm) GetMgoQuery() bson.M {
	qt := strings.Split(qb.Typ, "+") //%2B
	var ids []bson.ObjectId
	for _, t := range qt {
		id, err := doc.GetAccTermId(qb.CompanyId, t)
		if err != nil {
			continue
		}
		ids = append(ids, id)
	}
	fmt.Println(ids)
	return bson.M{
		"_id": bson.M{"$in": ids},
	}
}

func (pa *PutAccTermCatagory) GetAccTermCatagoryDoc() *doc.AccTermList {
	m := &doc.AccTermList{
		ID:     pa.ID,
		Enable: pa.Enable,
	}
	return m
}

func (pa *PutAccTermItem) GetAccTermItemDoc() *doc.AccTermList {
	m := &doc.AccTermList{
		ID:     pa.ID,
		Typ:    pa.Typ,
		Enable: pa.Enable,
	}
	var termGroup []*doc.AccTermGroup
	for _, c := range pa.Terms {

		var subItem []*doc.AccTerm
		for _, sub := range c.Sub {
			subItem = append(subItem, &doc.AccTerm{
				Name:   sub.Name,
				Code:   sub.Code,
				Desc:   sub.Desc,
				Enable: sub.Enable,
			})
		}
		termGroup = append(termGroup, &doc.AccTermGroup{
			Name:   c.Name,
			Code:   c.Code,
			Desc:   c.Desc,
			Enable: c.Enable,
			Sub:    subItem,
		})
	}
	m.Terms = termGroup
	return m
}
