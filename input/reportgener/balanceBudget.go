package input

import (
	"encoding/json"
	"errors"
	"kalimasi/doc"
	"kalimasi/model/reportgener"

	"github.com/globalsign/mgo/bson"
)

type PutSettingBalanceBudget struct {
	ID                   bson.ObjectId
	SettingBalanceBudget []*reportgener.SettingBalanceBudget
}

func (pb *PutSettingBalanceBudget) Validate() error {
	var err error
	for _, p := range pb.SettingBalanceBudget {

		if p.Category == "" {
			return errors.New("invalid category")
		}

		err = p.Validate()
		if err != nil {
			return err
		}
	}
	return err

}

func (pi *PutSettingBalanceBudget) GetReportDoc() *doc.Report {

	b, _ := json.Marshal(pi.SettingBalanceBudget)
	data := string(b)

	r := &doc.Report{
		JsonSetting: data,
	}

	return r
}
