/*
 * 憑證管理
 */
package doc

import (
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	receiptC = "receipt"

	TypeReceiptElectronic = "electronic" // 電子發票
	TypeReceiptTriplicate = "triplicate" // 三聯發票
	TypeReceiptDuplicate  = "duplicate"  // 二聯發票
	TypeReceiptInvoice    = "invoice"    // 憑證
	TypeReceiptCustoms    = "customs"    // 海關

	TypeTaxTable = "tax"
	TypeTaxFree  = "free" // 免稅
	TypeTaxZero  = "zero" // 零稅
	TypeTaxNone  = "none" // 無法扣抵

	TypeAccIncome = "income"
	TypeAccExpand = "expenses"
)

type Tax struct {
	Typ       string
	Amount    int // 使用者輸入稅額
	SysAmount int // 系統計算稅額
	Rate      int
}

type Receipt struct {
	ID              bson.ObjectId
	DateTime        time.Time
	Year            int
	Typ             string // 電子發票 / 三聯 / 二聯 / 憑證 / 海關
	TypeAcc         string // 進項 / 銷項
	Number          string // 發票號碼
	VATnumber       string // 統一編號
	Amount          int
	IsCollect       bool // 是否為匯總
	CollectQuantity int
	IsFixedAsset    bool

	Pictures []string // 上傳圖片清單
	TaxInfo  Tax

	CompanyID bson.ObjectId
	BudgetId  *bson.ObjectId

	DebitAccTerm  []*MiniAccount
	CreditAccTerm []*MiniAccount

	CommonDoc `bson:"meta"`
}

func (r *Receipt) GetDebitAccTerm(id bson.ObjectId) *MiniAccount {
	for _, d := range r.DebitAccTerm {
		if d.ID == id {
			return d
		}
	}
	return nil
}

func (r *Receipt) GetCreditAccTerm(id bson.ObjectId) *MiniAccount {
	for _, d := range r.CreditAccTerm {
		if d.ID == id {
			return d
		}
	}
	return nil
}

func (r *Receipt) GetDoc() interface{} {
	if !r.ID.Valid() {
		r.ID = r.newID()
	}
	r.Year = r.DateTime.Year() - 1911
	return r
}

func (r *Receipt) GetID() bson.ObjectId {
	return r.ID
}

func (r *Receipt) GetC() string {
	if !r.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(r.CompanyID.Hex(), receiptC)
}

func (u *Receipt) SetCompany(c bson.ObjectId) {
	u.CompanyID = c
}

func (u *Receipt) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *Receipt) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *Receipt) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (r *Receipt) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"companyid", "year", "typeacc"},
			Background: true, // can be used while index is being built
		},
	}
}
