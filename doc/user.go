package doc

import (
	"errors"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	UserPermManage = "manage"
	UserPermUser   = "user"

	userC        = "user"
	userCompanyC = "userCompany"
)

type User struct {
	ID            bson.ObjectId `bson:"_id"`
	Email         string
	EmailVerified bool `bson:"-"`
	PhoneNumber   string
	Pwd           string
	DisplayName   string
	Disabled      bool
	Permission    string
	RegistTime    time.Time

	CompanyPerm []*UserCompanyPerm `bson:"lookupUCP"`

	CommonDoc `bson:"-"`
}

func (u *User) GetC() string {
	return userC
}
func (u *User) GetDoc() interface{} {
	u.ID = bson.NewObjectId()
	return u
}
func (u *User) GetID() bson.ObjectId {
	return u.ID
}
func (u *User) GetUpdateField() bson.M {
	return bson.M{
		"permission": u.Permission,
	}
}
func (u *User) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *User) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *User) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (u *User) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"email", "pwd"},
			Background: true, // can be used while index is being built
		},
	}
}

func (u *User) GetAcc() string {
	return u.Email
}

func (u *User) GetName() string {
	return u.DisplayName
}

func (u *User) GetUserCompanyPerm(unitCode string, isFake bool) (*UserCompanyPerm, error) {
	cid, err := getCompanyId(unitCode, isFake)
	if err != nil {
		return nil, err
	}
	for _, c := range u.CompanyPerm {
		if c.CompanyID == cid {
			return c, nil
		}
	}
	return nil, errors.New("company not found")
}

func (u *User) GetPipeline(q bson.M) PipelineQryInter {
	return &CommonPipeline{
		q: q,
		pipeline: []bson.M{
			bson.M{"$match": q},
			bson.M{
				"$lookup": bson.M{
					"from":         userCompanyC,
					"localField":   "_id",
					"foreignField": "userid",
					"as":           "lookupUCP",
				},
			},
		},
	}

}

const (
	UserCompanyPermEdit   = "edit"
	UserCompanyPermExpert = "expert"
)

type UserCompanyPerm struct {
	ID         bson.ObjectId
	UserID     bson.ObjectId
	CompanyID  bson.ObjectId
	Permission string

	CommonDoc
}

func (u *UserCompanyPerm) GetC() string {
	return userCompanyC
}

func (u *UserCompanyPerm) GetDoc() interface{} {
	u.ID = bson.NewObjectId()
	return u
}

func (u *UserCompanyPerm) GetID() bson.ObjectId {
	return u.ID
}

func (u *UserCompanyPerm) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *UserCompanyPerm) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *UserCompanyPerm) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}
