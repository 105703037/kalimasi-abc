package doc

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	reportC = "report"
)

const (
	BalanceBudget = "balanceBudget" // 預算表設定
	FinalAccount  = "finalAccount"  // 決算表設定
	BalanceSheet  = "balanceSheet"
)

var (
	reportTypeMap = map[string]uint8{
		BalanceBudget: 1, // 預算表設定
		FinalAccount:  1, // 決算表設定
		BalanceSheet:  2,
	}
	allReportType []string
)

type Report struct {
	ID          bson.ObjectId `bson:"_id"`
	Typ         string
	CompanyID   bson.ObjectId
	JsonSetting string

	CommonDoc `bson:"meta"`
}

func (r *Report) GetDoc() interface{} {
	if !r.ID.Valid() {
		r.ID = r.newID()
	}
	return r
}

func GetAllReportType() []string {
	if len(reportTypeMap) != len(allReportType) {
		for k := range reportTypeMap {
			allReportType = append(allReportType, k)
		}
	}
	return allReportType
}

func GetReportID(typ string, companyID bson.ObjectId) bson.ObjectId {
	var b [12]byte
	orgByte := []byte(string(companyID)[0:8])
	for i := 0; i < 8; i++ {
		b[i] = orgByte[i]
	}

	code, ok := reportTypeMap[typ]
	if !ok {
		panic("invaid report type")
	}
	b[8] = byte(code)
	return bson.ObjectId(b[:])
}

func (d *Report) newID() bson.ObjectId {
	return GetReportID(d.Typ, d.CompanyID)
}

func (r *Report) GetID() bson.ObjectId {
	return r.ID
}

func (r *Report) GetC() string {
	return reportC
}

func (u *Report) SetCompany(c bson.ObjectId) {
	u.CompanyID = c
}

func (u *Report) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *Report) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *Report) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (r *Report) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"companyid", "typ"},
			Background: true, // can be used while index is being built
		},
	}
}

func (r *Report) GetUpdateField() bson.M {
	return bson.M{
		"jsonsetting": r.JsonSetting,
	}
}
