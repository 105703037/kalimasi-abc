package doc

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_taxFileGetID(t *testing.T) {
	c := Company{
		UnitCode: "12345678",
	}
	oid := c.newID()
	fmt.Println(oid)
	tf := TaxFile{CompanyID: oid, Year: 2007}
	tid := tf.newID()
	fmt.Println(tid)
	assert.True(t, false)
}
