package doc

import (
	"encoding/binary"
	"encoding/json"
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

const (
	logC = "log"

	ActCreate = "create"
	ActUpdate = "update"
	ActDelete = "delete"
)

type LogUser interface {
	GetName() string
	GetAcc() string
	GetCompany() bson.ObjectId
}

type log struct {
	ID         bson.ObjectId `bson:"_id"`
	C          string
	DocID      bson.ObjectId
	Act        string
	Name       string
	Account    string
	OldData    string
	CompanyID  bson.ObjectId
	CreateTime time.Time
	CommonDoc  `bson:"-"`
}

func GetDocLog(d DocInter, u LogUser, act string) DocInter {
	now := time.Now()
	var data string
	if act != ActCreate {
		b, _ := json.Marshal(d)
		data = string(b)
	}

	return &log{
		C:          d.GetC(),
		DocID:      d.GetID(),
		CompanyID:  u.GetCompany(),
		Act:        act,
		Name:       u.GetName(),
		Account:    u.GetAcc(),
		OldData:    data,
		CreateTime: now,
	}
}

func (d *log) newID() bson.ObjectId {
	tu := d.CreateTime.Unix()
	if tu == 0 {
		panic("invalid createTime")
	}
	var b [12]byte
	// Timestamp, 4 bytes, big endian
	binary.BigEndian.PutUint32(b[:], uint32(tu))

	cid := []byte(d.C)
	l := len(cid)
	for i := 0; i < l; i++ {
		if i == 8 {
			break
		}
		b[4+i] = cid[i]
	}
	return bson.ObjectId(b[:])
}

func (u *log) GetC() string {
	if !u.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(u.CompanyID.Hex(), logC)
}

func (u *log) GetID() bson.ObjectId {
	return u.ID
}

func (u *log) GetDoc() interface{} {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u
}

func (u *log) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *log) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *log) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}

func (u *log) GetMongoIndexes() []mgo.Index {
	return []mgo.Index{
		{
			Key:        []string{"c", "docid"},
			Background: true, // can be used while index is being built
		},
		{
			Key:         []string{"createtime"},
			Background:  true,             // can be used while index is being built
			ExpireAfter: time.Hour * 8760, // 一年後刪掉
		},
	}
}
