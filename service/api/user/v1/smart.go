package v1

import "kalimasi/rsrc"

type SmartAPI string

func (api SmartAPI) GetName() string {
	return string(api)
}

func (a SmartAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{}
}

func (a SmartAPI) Init() {
}
