package v1

import (
	"encoding/json"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"
)

type RebateAPI string

func (api RebateAPI) GetName() string {
	return string(api)
}

func (a RebateAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		&rsrc.APIHandler{Path: "/v1/rebate", Next: a.createEndpoint, Method: "POST", Auth: true},
		&rsrc.APIHandler{Path: "/v1/rebate", Next: a.getEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/rebate/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/rebate/{ID}", Next: a.modifyEndpoint, Method: "PUT", Auth: true},
		&rsrc.APIHandler{Path: "/v1/rebate/{ID}", Next: a.delEndpoint, Method: "DELETE", Auth: true},
	}
}

func (a RebateAPI) Init() {

}
func (api *RebateAPI) delEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	bs := &doc.Rebate{ID: qid, CompanyID: ui.CompanyID}
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetRebateModel(dbclt)
	err = bm.Delete(bs, ui)
	if err != nil {
		switch err.Error() {
		case "not found":
			w.WriteHeader(http.StatusNotFound)
		default:
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}
	w.Write([]byte("ok"))
}

func (api *RebateAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	bs := &doc.Rebate{ID: qid, CompanyID: ui.CompanyID}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(bs)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}

	out, _ := doc.Format(bs, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Rebate); ok {
			bid := ""
			if d.BudgetId != nil {
				bid = d.BudgetId.Hex()
			}
			rid := ""
			if d.ReceiptID != nil {
				rid = d.ReceiptID.Hex()
			}
			var debit []map[string]interface{}
			for _, da := range d.DebitAccTerm {
				debit = append(debit, map[string]interface{}{
					"id":     da.ID.Hex(),
					"code":   da.Code,
					"name":   da.Name,
					"amount": da.Amount,
					"desc":   da.Desc,
				})
			}
			var credit []map[string]interface{}
			for _, da := range d.CreditAccTerm {
				credit = append(credit, map[string]interface{}{
					"id":     da.ID.Hex(),
					"code":   da.Code,
					"name":   da.Name,
					"amount": da.Amount,
					"desc":   da.Desc,
				})
			}
			return map[string]interface{}{
				"id":        d.ID.Hex(),
				"budgetId":  bid,
				"receiptId": rid,
				"accType":   d.TypeAcc,
				"rDate":     d.DateTime.Format(time.RFC3339),

				"amount": d.Amount,

				"tax": map[string]interface{}{
					"type":   d.TaxInfo.Typ,
					"amount": d.TaxInfo.Amount,
					"rate":   d.TaxInfo.Rate,
				},
				"debitAccTermList":  debit,
				"creditAccTermList": credit,
			}
		}
		return nil
	})
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *RebateAPI) modifyEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.PutRebate{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil || qid != cb.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetRebateModel(dbclt)
	err = bm.Modify(cb, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *RebateAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"page"}, true)
	ui := input.GetUserInfo(req)
	// qb := input.QueryReceipt{
	// 	AccType:   qv["at"].(string),
	// 	Typ:       qv["t"].(string),
	// 	Number:    qv["number"].(string),
	// 	BudgetID:  qv["budget"].(string),
	// 	StartDate: qv["sd"].(string),
	// 	EndDate:   qv["ed"].(string),
	// 	CompanyID: ui.CompanyID,
	// }

	// if err := qb.Validate(); err != nil {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	w.Write([]byte(err.Error()))
	// 	return
	// }

	page, err := strconv.Atoi(qv["page"].(string))
	if err != nil {
		page = 1
	}
	q := bson.M{}
	//	q := qb.GetMgoQuery()
	mgodb := model.GetMgoDBModelByReq(req)
	out, err := mgodb.Paginator(&doc.Rebate{CompanyID: ui.CompanyID}, q, 200, page, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Rebate); ok {
			return map[string]interface{}{
				"id":        d.ID.Hex(),
				"accType":   d.TypeAcc,
				"rDate":     d.DateTime.Format(time.RFC3339),
				"amount":    d.Amount,
				"taxType":   d.TaxInfo.Typ,
				"taxAmount": d.TaxInfo.Amount,
			}
		}
		return nil
	})
	if err != nil {
		if err.Error() == "query data is 0" {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *RebateAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	cr := &input.CreateRebate{}
	err := json.NewDecoder(req.Body).Decode(cr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cr.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	rm := model.GetRebateModel(dbclt)
	err = rm.Create(cr, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}
