package v1

import (
	"encoding/json"
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"
)

type AccTermAPI string

func (api AccTermAPI) GetName() string {
	return string(api)
}

func (a AccTermAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		&rsrc.APIHandler{Path: "/v1/accTerm", Next: a.getEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/accTerm/{CODE}/ref", Next: a.getRefEndpoint, Method: "GET", Auth: true},

		&rsrc.APIHandler{Path: "/v1/accTerm/category", Next: a.getAccTermCategoryEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/accTerm/category/{ID}", Next: a.modifyAccTermCategoryEndpoint, Method: "PUT", Auth: true},

		&rsrc.APIHandler{Path: "/v1/accTermItem", Next: a.getAccTermItemEndpoint, Method: "GET", Auth: true},
		//&rsrc.APIHandler{Path: "/v1/accTermItem", Next: a.createAccTermEndpoint, Method: "POST", Auth: true},
		&rsrc.APIHandler{Path: "/v1/accTermItem/{ID}", Next: a.modifyAccTermEndpoint, Method: "PUT", Auth: true},
	}
}

func (a AccTermAPI) Init() {

}

func (api *AccTermAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"t"}, true)
	ui := input.GetUserInfo(req)
	input := input.QueryAccTerm{Typ: qv["t"].(string), CompanyId: ui.CompanyID}
	mgodb := model.GetMgoDBModelByReq(req)
	q := input.GetMgoQuery()
	result, err := mgodb.Find(&doc.AccTermList{}, q, 0, 0)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	atAry := result.([]*doc.AccTermList)

	if len(atAry) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	var out []map[string]interface{}
	for _, at := range atAry {
		for _, t := range at.Terms {
			out = append(out, map[string]interface{}{
				"code": t.Code,
				"name": t.Name,
			})
			if len(t.Sub) == 0 {
				continue
			}
			for _, s := range t.Sub {
				out = append(out, map[string]interface{}{
					"code": s.Code,
					"name": s.Name,
				})
			}
		}
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AccTermAPI) getRefEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"CODE"})
	queryCode := vars["CODE"].(string)
	if _, err := util.IsInt(queryCode, nil); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	am := model.GetAccTermModel(dbclt)
	result := am.Refrence(ui.CompanyID, queryCode)
	if result == nil {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(map[string]interface{}{
		"reports":         result.RefReport,
		"isCompanyPay":    result.HasCompanyPayMethod,
		"isCompanyIncome": result.HasCompanyIncome,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AccTermAPI) modifyAccTermEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &input.PutAccTermItem{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	if err != nil || qid != ca.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	am := model.GetAccTermModel(dbclt)
	err = am.Modify(ca, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *AccTermAPI) getAccTermItemEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"t"}, true)
	ui := input.GetUserInfo(req)
	input := input.QueryAccTerm{Typ: qv["t"].(string), CompanyId: ui.CompanyID}
	mgodb := model.GetMgoDBModelByReq(req)
	//使用CompanyID和accterm type找到accterm id
	at := &doc.AccTermList{Typ: input.Typ, CompanyID: ui.CompanyID}
	atId, err := doc.GetAccTermId(ui.CompanyID, input.Typ)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	at.ID = atId
	err = mgodb.FindByID(at)
	if err != nil {
		w.WriteHeader(http.StatusNoContent)
		w.Write([]byte(err.Error()))
		return
	}

	out, _ := doc.Format(at, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.AccTermList); ok {
			//TODO::
			/**can change struct here**/
			for i, tmp := range d.Terms {
				if tmp.Sub == nil {
					d.Terms[i].Sub = []*doc.AccTerm{}
				}
			}

			return map[string]interface{}{
				"id":     d.ID.Hex(),
				"typ":    d.Typ,
				"terms":  d.Terms,
				"enable": d.Enable,
			}
		}
		return nil
	})

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AccTermAPI) getAccTermCategoryEndpoint(w http.ResponseWriter, req *http.Request) {
	const plus = "+"
	//qv := doc.TypeAccTermAssets + "+" + doc.TypeAccTermDebt + "+" + doc.TypeAccTermFund + "+" + doc.TypeAccTermIncome + "+" + doc.TypeAccTermCost + "+" + doc.TypeAccTermExpenses
	qv := util.StrAppend(doc.TypeAccTermAssets, plus, doc.TypeAccTermDebt, plus, doc.TypeAccTermFund, plus, doc.TypeAccTermIncome, plus, doc.TypeAccTermCost, plus, doc.TypeAccTermExpenses)
	ui := input.GetUserInfo(req)
	minput := input.QueryAccTerm{Typ: qv, CompanyId: ui.CompanyID}
	mgodb := model.GetMgoDBModelByReq(req)
	q := minput.GetMgoQuery()

	result, err := mgodb.Find(&doc.AccTermList{}, q, 0, 0)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	atAry := result.([]*doc.AccTermList)

	if len(atAry) != 6 {
		initCA := []*input.CreateAccTermItem{}
		initCA = append(initCA,
			&input.CreateAccTermItem{Typ: doc.TypeAccTermAssets},
			&input.CreateAccTermItem{Typ: doc.TypeAccTermFund},
			&input.CreateAccTermItem{Typ: doc.TypeAccTermIncome},
			&input.CreateAccTermItem{Typ: doc.TypeAccTermCost},
			&input.CreateAccTermItem{Typ: doc.TypeAccTermDebt},
			&input.CreateAccTermItem{Typ: doc.TypeAccTermExpenses})

		dbclt := rsrc.GetDI().GetMongoByReq(req)
		am := model.GetAccTermModel(dbclt)
		for _, ca := range initCA {
			err = am.CreateAccTerm(ca, ui)
			if err != nil {
				//可能資料庫一開始不是空的，反正只建立空資料
				fmt.Println(err.Error())
			}
		}

		result, err := mgodb.Find(&doc.AccTermList{}, q, 0, 0)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		atAry = result.([]*doc.AccTermList)
		//預設建立資料，找不到資料的話則出錯
		if len(atAry) != 6 {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("something error"))
			return
		}
	}

	var out []map[string]interface{}
	for _, at := range atAry {
		out = append(out, map[string]interface{}{
			"id":     at.ID.Hex(),
			"enable": at.Enable,
			"typ":    at.Typ,
		})
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AccTermAPI) modifyAccTermCategoryEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &input.PutAccTermCatagory{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	if err != nil || qid != ca.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	am := model.GetAccTermModel(dbclt)
	err = am.ModifyCatagory(ca, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *AccTermAPI) createAccTermEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &input.CreateAccTermItem{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	am := model.GetAccTermModel(dbclt)

	err = am.CreateAccTerm(ca, ui)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

// out, err := json.Marshal(ui)
// if err != nil {
// 	fmt.Println(err)
// }
// fmt.Println("ui:", string(out))
