package v1

import (
	"encoding/json"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"
	"time"

	"github.com/globalsign/mgo/bson"
)

type BudgetAPI string

func (api BudgetAPI) GetName() string {
	return string(api)
}

func (a BudgetAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		&rsrc.APIHandler{Path: "/v1/budget", Next: a.getEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/budget", Next: a.createEndpoint, Method: "POST", Auth: true},
		&rsrc.APIHandler{Path: "/v1/budget/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/budget/{ID}", Next: a.modifyEndpoint, Method: "PUT", Auth: true},
		&rsrc.APIHandler{Path: "/v1/budget/{ID}", Next: a.delEndpoint, Method: "DELETE", Auth: true},
	}
}

func (a BudgetAPI) Init() {

}

func (api *BudgetAPI) delEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	bs := &doc.BudgetStatement{ID: qid, CompanyID: ui.CompanyID}
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetBudgetModel(dbclt)
	err = bm.Delete(bs, ui)
	if err != nil {
		switch err.Error() {
		case "has project":
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("can not delete: has project"))
		case "not found":
			w.WriteHeader(http.StatusNotFound)
		default:
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}
	w.Write([]byte("ok"))
}

func (api *BudgetAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"type", "year", "title"}, true)
	ui := input.GetUserInfo(req)
	qb := input.QueryBudget{
		Year:      qv["year"].(string),
		Typ:       qv["type"].(string),
		Title:     qv["title"].(string),
		CompanyID: ui.CompanyID,
	}
	if err := qb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	q := qb.GetMgoQuery()
	mgodb := model.GetMgoDBModelByReq(req)
	bs := &doc.BudgetStatement{
		CompanyID: ui.CompanyID,
	}
	result, err := mgodb.PipelineAll(bs, bs.GetPipeline(q).GetPipeline())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	out, c := doc.Format(result, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.BudgetStatement); ok {
			incomeT, expensesT := 0, 0
			for _, i := range d.Income {
				incomeT += i.Amount
			}
			for _, e := range d.Expenses {
				expensesT += e.Amount
			}
			return map[string]interface{}{
				"id":           d.ID.Hex(),
				"year":         d.Year,
				"title":        d.Title,
				"income":       incomeT,
				"expenses":     expensesT,
				"projectCount": len(d.ProjectList),
				"total":        incomeT - expensesT,
				"startDate":    d.StartDate.Format(time.RFC3339),
				"endDate":      d.EndDate.Format(time.RFC3339),
				"createTime":   d.CreateTime.Format(time.RFC3339),
				"creator":      d.Creator,
			}
		}
		return nil
	})

	if c == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *BudgetAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.CreateBudget{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetBudgetModel(dbclt)
	err = bm.Create(cb, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *BudgetAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)

	bs := &doc.BudgetStatement{ID: qid, CompanyID: ui.CompanyID}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(bs)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}
	out, _ := doc.Format(bs, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.BudgetStatement); ok {
			incomeT, expensesT := 0, 0
			for _, i := range d.Income {
				incomeT += i.Amount
			}
			for _, e := range d.Expenses {
				expensesT += e.Amount
			}
			parentId := ""
			if bs.ParentID != nil {
				parentId = bs.ParentID.Hex()
			}
			sub, err := mgoDB.Find(&doc.BudgetStatement{CompanyID: ui.CompanyID}, bson.M{"parentid": d.ID}, 0, 0)
			subProject := make([]map[string]interface{}, 0)
			if err == nil {
				pl := sub.([]*doc.BudgetStatement)
				for _, b := range pl {
					subProject = append(subProject, map[string]interface{}{
						"id":        b.ID.Hex(),
						"name":      b.Title,
						"startDate": b.StartDate.Format(time.RFC3339),
						"endDate":   b.EndDate.Format(time.RFC3339),
						"amount":    b.Amount,
					})
				}
			}
			return map[string]interface{}{
				"id":         d.ID.Hex(),
				"year":       d.Year,
				"title":      d.Title,
				"type":       d.Typ,
				"parentId":   parentId,
				"amount":     d.Amount,
				"income":     d.Income,
				"expenses":   d.Expenses,
				"startDate":  d.StartDate.Format(time.RFC3339),
				"endDate":    d.EndDate.Format(time.RFC3339),
				"createTime": d.CreateTime.Format(time.RFC3339),
				"creator":    d.Creator,
				"project":    subProject,
			}
		}
		return nil
	})
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *BudgetAPI) modifyEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.PutBudget{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil || qid != cb.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetBudgetModel(dbclt)
	err = bm.Modify(cb, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}
