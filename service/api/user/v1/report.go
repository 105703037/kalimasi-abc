package v1

import (
	"encoding/json"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/model/reportgener"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"
	"time"
)

const (
	ReportFormatPDF  = "pdf"
	ReportFormatJson = "json"
)

type ReportAPI string

func (api ReportAPI) GetName() string {
	return string(api)
}

func (a ReportAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		&rsrc.APIHandler{Path: "/v1/report/budget", Next: a.budgetEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/report/finalAccount", Next: a.finalAccountEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/report/balanceSheet", Next: a.balanceSheetEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/report/taxCal", Next: a.taxCalEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/report/taxCal", Next: a.createTaxCalEndpoint, Method: "POST", Auth: true},
		&rsrc.APIHandler{Path: "/v1/report/enterIncome", Next: a.enterIncomeEndpoint, Method: "GET", Auth: true},
	}
}

func (a ReportAPI) Init() {
}

func (api *ReportAPI) enterIncomeEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"sd", "ed", "f"}, true)
	ui := input.GetUserInfo(req)

	qb := input.QueryReceipt{
		StartDate: qv["sd"].(string),
		EndDate:   qv["ed"].(string),
		CompanyID: ui.CompanyID,
	}

	if err := qb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	mgodb := model.GetMgoDBModelByReq(req)
	c := &doc.Company{
		ID: ui.CompanyID,
	}
	err := mgodb.FindByID(c)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("company not found "))
		return
	}

	if c.TaxInfo == nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("must set company tax info"))
		return
	}

	q := qb.GetMgoQuery()

	di := rsrc.GetDI()
	mdb := di.GetMongoByReq(req)
	dbmodel := model.GetMgoDBModel(mdb)
	bb := &reportgener.EnterIncomeTaxGen{
		ReceiptDao: dbmodel.Iter(&doc.Receipt{
			CompanyID: ui.CompanyID,
		}, q),
		VATnumber:   c.UnitCode,
		TaxNumber:   c.TaxInfo.Code,
		CompanyID:   ui.CompanyID,
		CompanyName: c.Name,
	}

	rm := model.GetReportModel(mdb)
	if qv["f"].(string) == ReportFormatJson {
		w.Header().Set("Content-Type", "application/json")
		err := rm.OutputJSON(bb, w)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
	} else {
		url, err := rm.GenerateTXT(bb, nil)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("generate pdf error: " + err.Error()))
			return
		}
		http.Redirect(w, req, url, http.StatusMovedPermanently)
	}

}

func (api *ReportAPI) budgetEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"current", "last", "foreignField", "f"}, true)
	ui := input.GetUserInfo(req)
	input := input.QueryBudgetReport{
		Current: qv["current"].(string),
		Last:    qv["last"].(string)}
	if err := input.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	q := input.GetMgoQuery()
	mgodb := model.GetMgoDBModelByReq(req)
	bs := &doc.BudgetStatement{
		CompanyID: ui.CompanyID,
	}
	pqi := bs.GetPipeline(q)
	result, err := mgodb.PipelineAll(bs, pqi.GetPipeline())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	var currentBudget, lastBudget *doc.BudgetStatement
	if budgetList, ok := result.([]*doc.BudgetStatement); ok {
		lbl := len(budgetList)
		if lbl == 0 {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("budget not found "))
			return
		}
		if lbl == 1 {
			currentBudget = budgetList[0]
			lastBudget = nil
		}
		if lbl == 2 {
			if input.Current == budgetList[0].ID.Hex() {
				currentBudget = budgetList[0]
				lastBudget = budgetList[1]
			} else {
				currentBudget = budgetList[1]
				lastBudget = budgetList[0]
			}
		}
	}

	c := &doc.Company{
		ID: currentBudget.CompanyID,
	}
	err = mgodb.FindByID(c)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("company not found "))
		return
	}

	bb := &reportgener.BalanceBudget{
		Title:     c.Name,
		Year:      currentBudget.Year,
		CompanyID: currentBudget.CompanyID,
	}
	bb.SetCurrentBudget(currentBudget)
	bb.SetLastBudget(lastBudget)
	di := rsrc.GetDI()
	mdb := di.GetMongoByReq(req)
	rm := model.GetReportModel(mdb)
	if err = rm.LoadSetting(bb); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("setting not found: " + err.Error()))
		return
	}

	if qv["f"].(string) == ReportFormatPDF {
		url, err := rm.GeneratePDF(bb, nil)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("generate pdf error: " + err.Error()))
			return
		}
		http.Redirect(w, req, url, http.StatusMovedPermanently)
	} else {
		w.Header().Set("Content-Type", "application/json")
		err := rm.OutputJSON(bb, w)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
	}

}

func (api *ReportAPI) finalAccountEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"budget", "f"}, true)
	ui := input.GetUserInfo(req)
	input := input.QueryFinalAccountReport{
		Budget: qv["budget"].(string)}
	if err := input.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	q := input.GetMgoQuery()
	mgodb := model.GetMgoDBModelByReq(req)
	bs := &doc.BudgetStatement{
		CompanyID: ui.CompanyID,
	}
	err := mgodb.FindOne(bs, q)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	di := rsrc.GetDI()
	mdb := di.GetMongoByReq(req)

	am := model.GetAccountModel(mdb)
	fa, err := am.GetFinalAccount(bs.CompanyID, bs.Year)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	c := &doc.Company{
		ID: bs.CompanyID,
	}
	err = mgodb.FindByID(c)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("company not found "))
		return
	}

	bb := &reportgener.FinalAccount{
		BalanceBudget: reportgener.BalanceBudget{
			Title:     c.Name,
			Year:      bs.Year,
			CompanyID: bs.CompanyID,
		},
	}
	bb.SetCurrentBudget(fa)
	bb.SetLastBudget(bs)

	rm := model.GetReportModel(mdb)
	if err = rm.LoadSetting(bb); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if qv["f"].(string) == ReportFormatPDF {
		url, err := rm.GeneratePDF(bb, nil)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("generate pdf error: " + err.Error()))
			return
		}
		http.Redirect(w, req, url, http.StatusMovedPermanently)
	} else {
		w.Header().Set("Content-Type", "application/json")
		err := rm.OutputJSON(bb, w)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
	}
}

func (api *ReportAPI) balanceSheetEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"year", "f"}, true)
	ui := input.GetUserInfo(req)
	input := input.QueryTaxCalReport{
		Year: qv["year"].(string)}
	if err := input.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	// 抓結帳資料
	di := rsrc.GetDI()
	mdb := di.GetMongoByReq(req)
	mgodb := model.GetMgoDBModel(mdb)

	c := &doc.Company{
		ID: ui.CompanyID,
	}
	err := mgodb.FindByID(c)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("company not found "))
		return
	}

	bb := &reportgener.BalanceSheet{
		Title:     c.Name,
		CompanyID: ui.CompanyID,
		Year:      input.GetYear(),
	}

	am := model.GetAccountModel(mdb)
	fa, err := am.GetFinalAccount(ui.CompanyID, input.GetYear())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	bb.SetData(fa)

	rm := model.GetReportModel(mdb)
	if err = rm.LoadSetting(bb); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if qv["f"].(string) == ReportFormatPDF {
		url, err := rm.GeneratePDF(bb, nil)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		http.Redirect(w, req, url, http.StatusMovedPermanently)
	} else {
		w.Header().Set("Content-Type", "application/json")
		err := rm.OutputJSON(bb, w)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
	}

}

func (api *ReportAPI) taxCalEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"year", "f"}, true)
	ui := input.GetUserInfo(req)
	input := input.QueryTaxCalReport{
		Year: qv["year"].(string)}
	if err := input.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	// 抓結帳資料
	di := rsrc.GetDI()
	mdb := di.GetMongoByReq(req)
	mgodb := model.GetMgoDBModel(mdb)

	c := &doc.Company{
		ID: ui.CompanyID,
	}
	err := mgodb.FindByID(c)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("company not found "))
		return
	}

	// 資料庫查上次資料
	oldD := &doc.TaxFile{
		Year:      input.GetYear(),
		CompanyID: ui.CompanyID,
	}
	oldD.ID = oldD.GetID()
	err = mgodb.FindByID(oldD)
	oldD.CompanyObj = c
	var bb model.ReportGenInter
	if err == nil {
		bb = &reportgener.TaxFileDocTrans{
			Doc: oldD,
		}
	} else {
		am := model.GetAccountModel(mdb)
		fa, err := am.GetFinalAccount(ui.CompanyID, input.GetYear())
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("get final account err: " + err.Error()))
			return
		}

		id, err := doc.GetAccTermId(ui.CompanyID, doc.TypeAccTermIncome)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("get accter error: " + err.Error()))
			return
		}
		incomes := doc.AccTermList{
			ID: id,
		}
		err = mgodb.FindByID(&incomes)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("acc term incomes not found "))
			return
		}
		id, err = doc.GetAccTermId(ui.CompanyID, doc.TypeAccTermExpenses)
		expenses := doc.AccTermList{
			ID: id,
		}
		err = mgodb.FindByID(&expenses)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("acc term expenses not found "))
			return
		}
		now := time.Now()
		bb = &reportgener.TaxFileTrans{
			Year:       input.GetYear(),
			StartDate:  time.Date(1911+input.GetYear(), 1, 1, 0, 0, 0, 0, now.Location()),
			EndDate:    time.Date(1911+input.GetYear(), 12, 31, 0, 0, 0, 0, now.Location()),
			Company:    c,
			Income:     incomes.GetAccTermAry(),
			Expenses:   expenses.GetAccTermAry(),
			DataAccess: fa,
		}
	}

	rm := model.GetReportModel(mdb)

	if qv["f"].(string) == ReportFormatPDF {
		url, err := rm.GeneratePDF(bb, nil)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		http.Redirect(w, req, url, http.StatusMovedPermanently)
	} else {
		w.Header().Set("Content-Type", "application/json")
		err := rm.OutputJSON(bb, w)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
	}

}

func (api *ReportAPI) createTaxCalEndpoint(w http.ResponseWriter, req *http.Request) {
	cr := &input.CreateTaxFile{}
	err := json.NewDecoder(req.Body).Decode(cr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cr.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	rm := model.GetMgoDBModel(dbclt)

	newD := cr.GetDoc(ui.CompanyID)
	oldD := &doc.TaxFile{
		ID:        newD.GetID(),
		CompanyID: ui.CompanyID,
	}
	err = rm.FindByID(oldD)
	if err == nil {
		err = rm.Update(newD, ui)
	} else {
		err = rm.Save(newD, ui)
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}
