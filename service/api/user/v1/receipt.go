package v1

import (
	"encoding/json"
	"fmt"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type ReceiptAPI string

func (api ReceiptAPI) GetName() string {
	return string(api)
}

func (a ReceiptAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		&rsrc.APIHandler{Path: "/v1/receipt", Next: a.createEndpoint, Method: "POST", Auth: true},
		&rsrc.APIHandler{Path: "/v1/receipt", Next: a.getEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/receipt/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/receipt/{ID}", Next: a.modifyEndpoint, Method: "PUT", Auth: true},
		&rsrc.APIHandler{Path: "/v1/receipt/{ID}", Next: a.delEndpoint, Method: "DELETE", Auth: true},
	}
}

func (a ReceiptAPI) Init() {

}
func (api *ReceiptAPI) delEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	bs := &doc.Receipt{ID: qid, CompanyID: ui.CompanyID}
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	bm := model.GetReceiptModel(dbclt, di.GetFBStorage())
	err = bm.Delete(bs, ui)
	if err != nil {
		switch err.Error() {
		case "not found":
			w.WriteHeader(http.StatusNotFound)
		default:
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}
	w.Write([]byte("ok"))
}

func (api *ReceiptAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	bs := &doc.Receipt{ID: qid, CompanyID: ui.CompanyID}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(bs)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}

	out, _ := doc.Format(bs, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Receipt); ok {
			bid := ""
			if d.BudgetId != nil {
				bid = d.BudgetId.Hex()
			}
			var debit []map[string]interface{}
			for _, da := range d.DebitAccTerm {
				debit = append(debit, map[string]interface{}{
					"id":     da.ID.Hex(),
					"code":   da.Code,
					"name":   da.Name,
					"amount": da.Amount,
					"desc":   da.Desc,
				})
			}
			var credit []map[string]interface{}
			for _, da := range d.CreditAccTerm {
				credit = append(credit, map[string]interface{}{
					"id":     da.ID.Hex(),
					"code":   da.Code,
					"name":   da.Name,
					"amount": da.Amount,
					"desc":   da.Desc,
				})
			}
			pictures := []map[string]interface{}{}
			for _, p := range d.Pictures {
				sp := strings.Split(p, "%2F")
				l := len(sp)
				sp[l-1] = util.StrAppend("thumb_", sp[l-1])
				pictures = append(pictures, map[string]interface{}{
					"origin": p,
					"thumb":  strings.Join(sp, "%2F"),
				})
			}
			return map[string]interface{}{
				"id":              d.ID.Hex(),
				"budgetId":        bid,
				"accType":         d.TypeAcc,
				"rDate":           d.DateTime.Format(time.RFC3339),
				"type":            d.Typ,
				"number":          d.Number,
				"VATnumber":       d.VATnumber,
				"amount":          d.Amount,
				"isCollect":       d.IsCollect,
				"collectQuantity": d.CollectQuantity,
				"isFixedAsset":    d.IsFixedAsset,
				"tax": map[string]interface{}{
					"type":   d.TaxInfo.Typ,
					"amount": d.TaxInfo.Amount,
					"rate":   d.TaxInfo.Rate,
				},
				"debitAccTermList":  debit,
				"creditAccTermList": credit,
				"pictures":          pictures,
			}
		}
		return nil
	})
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *ReceiptAPI) modifyEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.PutReceipt{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil || qid != cb.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	bm := model.GetReceiptModel(dbclt, di.GetFBStorage())
	err = bm.Modify(cb, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *ReceiptAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"at", "t", "number", "budget", "sd", "ed", "page", "limit"}, true)
	ui := input.GetUserInfo(req)
	qb := input.QueryReceipt{
		AccType:   qv["at"].(string),
		Typ:       qv["t"].(string),
		Number:    qv["number"].(string),
		BudgetID:  qv["budget"].(string),
		StartDate: qv["sd"].(string),
		EndDate:   qv["ed"].(string),
		CompanyID: ui.CompanyID,
	}

	if err := qb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	page, err := strconv.Atoi(qv["page"].(string))
	if err != nil {
		page = 1
	}

	limit, err := strconv.Atoi(qv["limit"].(string))
	if err != nil || limit > 200 {
		limit = 200
	}

	q := qb.GetMgoQuery()
	rsrc.GetDI().GetLog().Debug(fmt.Sprintln(q))
	mgodb := model.GetMgoDBModelByReq(req)
	out, err := mgodb.Paginator(&doc.Receipt{CompanyID: ui.CompanyID}, q, limit, page, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Receipt); ok {
			return map[string]interface{}{
				"id":        d.ID.Hex(),
				"accType":   d.TypeAcc,
				"date":      d.DateTime.Format(time.RFC3339),
				"number":    d.Number,
				"VATnumber": d.VATnumber,
				"amount":    d.Amount,
				"type":      d.Typ,
			}
		}
		return nil
	})
	if err != nil {
		if err.Error() == "query data is 0" {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *ReceiptAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	cr := &input.CreateReceipt{}
	err := json.NewDecoder(req.Body).Decode(cr)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cr.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	di := rsrc.GetDI()
	dbclt := di.GetMongoByReq(req)
	rm := model.GetReceiptModel(dbclt, di.GetFBStorage())
	err = rm.Create(cr, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}
