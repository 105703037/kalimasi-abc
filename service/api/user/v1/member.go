package v1

import (
	"encoding/json"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/globalsign/mgo/bson"
)

type MemberAPI string

func (api MemberAPI) GetName() string {
	return string(api)
}

func (a MemberAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		&rsrc.APIHandler{Path: "/v1/member", Next: a.createEndpoint, Method: "POST", Auth: true},
		&rsrc.APIHandler{Path: "/v1/member", Next: a.getEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/member/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/member/{ID}", Next: a.modifyEndpoint, Method: "PUT", Auth: true},
		&rsrc.APIHandler{Path: "/v1/member/{ID}/payment", Next: a.payEndpoint, Method: "POST", Auth: true},
		&rsrc.APIHandler{Path: "/v1/member/payment/{ID}", Next: a.deletePayEndpoint, Method: "DELETE", Auth: true},
	}
}

func (a MemberAPI) Init() {

}

func (api *MemberAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.CreateMember{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetMemberModel(dbclt)

	if bm.IsMemberExists(ui.CompanyID, cb.IDnumber, cb.Typ) {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("member exists"))
		return
	}

	err = bm.Create(cb, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *MemberAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"n", "m", "p", "page", "limit"}, true)
	ui := input.GetUserInfo(req)
	qb := input.QueryMember{
		Name:      qv["n"].(string),
		Email:     qv["m"].(string),
		Phone:     qv["p"].(string),
		CompanyID: ui.CompanyID,
	}

	if err := qb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	page, err := strconv.Atoi(qv["page"].(string))
	if err != nil {
		page = 1
	}

	limit, err := strconv.Atoi(qv["limit"].(string))
	if err != nil || limit > 200 {
		limit = 200
	}

	q := qb.GetMgoQuery()
	mgodb := model.GetMgoDBModelByReq(req)
	bs := &doc.Member{
		CompanyID: ui.CompanyID,
	}
	out, err := mgodb.Paginator(bs, q, limit, page, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Member); ok {
			l := len(d.Contact)
			pl := make([]string, l)
			for i := 0; i < l; i++ {
				pl[i] = d.Contact[i].Value
			}
			return map[string]interface{}{
				"id":    d.ID.Hex(),
				"name":  d.Name,
				"email": d.Email,
				"phone": strings.Join(pl, " / "),
			}
		}
		return nil
	})

	if err != nil {
		if err.Error() == "query data is 0" {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *MemberAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)

	bs := &doc.Member{ID: qid, CompanyID: ui.CompanyID}
	mgoDB := model.GetMgoDBModelByReq(req)
	pl := bs.GetPipeline(bson.M{"_id": bs.ID})
	err = mgoDB.PipelineOne(bs, pl.GetPipeline())
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}
	out, _ := doc.Format(bs, func(i interface{}) map[string]interface{} {
		if d, ok := i.(*doc.Member); ok {
			loc := time.Now().Location()
			var pr []map[string]interface{}
			for _, p := range d.PayRecord {
				pr = append(pr, map[string]interface{}{
					"id":            p.ID.Hex(),
					"payDate":       p.Date.In(loc).Format(time.RFC3339),
					"fee":           p.Name,
					"displayFee":    p.Display,
					"method":        p.Method,
					"displayMethod": p.DisplayMethod,
					"amount":        p.Amount,
					"desc":          p.Desc,
				})
			}
			return map[string]interface{}{
				"id":          d.ID.Hex(),
				"mType":       d.Typ,
				"mid":         d.IDnumber,
				"name":        d.Name,
				"sex":         d.Sex,
				"birthday":    d.Birthday.In(loc).Format(time.RFC3339),
				"email":       d.Email,
				"address":     d.Address,
				"serviceUnit": d.ServiceUnit,
				"contact":     d.Contact,
				"payRecord":   pr,
			}
		}
		return nil
	})
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *MemberAPI) modifyEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.PutMember{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil || qid != cb.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetMemberModel(dbclt)
	err = bm.Modify(cb, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *MemberAPI) deletePayEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetMemberModel(dbclt)
	err = bm.DeletePayment(qid, ui)
	if err != nil {
		switch err.Error() {
		case "not found":
			w.WriteHeader(http.StatusNotFound)
		default:
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}
	w.Write([]byte("ok"))
}

func (api *MemberAPI) payEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.CreatePayRecord{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	bm := model.GetMemberModel(dbclt)

	id, err := bm.AddPayRecord(qid, cb, ui)
	if err != nil {
		switch err.Error() {
		case "not found":
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("member not found"))
			return
		default:
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}

	}
	w.Write([]byte(id.Hex()))
}
