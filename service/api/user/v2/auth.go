package v2

import (
	"encoding/json"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/model"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"

	"github.com/globalsign/mgo/bson"
)

type AuthAPI string

func (api AuthAPI) GetName() string {
	return string(api)
}

func (a AuthAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		&rsrc.APIHandler{Path: "/v2/login", Next: a.loginEndpoint, Method: "POST", Auth: false},
		&rsrc.APIHandler{Path: "/v2/opToken", Next: a.opTokenEndpoint, Method: "GET", Auth: true},
	}
}

func (a AuthAPI) Init() {

}

func (api *AuthAPI) loginEndpoint(w http.ResponseWriter, req *http.Request) {
	cb := &input.LoginV2{}
	err := json.NewDecoder(req.Body).Decode(cb)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = cb.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	mgodb := rsrc.GetDI().GetMongoByReq(req)
	um := model.GetUserModel(mgodb)
	u, err := um.SigninV2(cb)
	if err != nil {
		switch err.Error() {
		case "company not found":
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte(err.Error()))
		case "not found":
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("account or password error"))
		default:
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"token": u,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *AuthAPI) opTokenEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)
	if !bson.IsObjectIdHex(ui.ID) {
		w.WriteHeader(http.StatusForbidden)
		return
	}
	uid := bson.ObjectIdHex(ui.ID)

	q := bson.M{"_id": uid}
	u := &doc.User{}
	mgodb := rsrc.GetDI().GetMongoByReq(req)
	mgoModel := model.GetMgoDBModel(mgodb)
	err := mgoModel.PipelineOne(u, u.GetPipeline(q).GetPipeline())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	if !u.ID.Valid() {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("not found"))
		return
	}

	qv := util.GetQueryValue(req, []string{"uc", "if"}, true)
	unitCode, _ := qv["uc"].(string)
	if unitCode == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("missing uc"))
		return
	}
	isFakeStr, _ := qv["if"].(string)
	isFake := (isFakeStr == "1")

	ucp, err := u.GetUserCompanyPerm(unitCode, isFake)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(err.Error()))
		return
	}
	t, err := rsrc.GetDI().GetJWTConf().GetToken(
		map[string]interface{}{
			"sub": u.ID.Hex(),
			"acc": u.Email,
			"nam": u.DisplayName,
			"per": ucp.Permission,
			"cat": ucp.CompanyID.Hex(),
		})

	c := &doc.Company{}
	err = mgoModel.FindOne(c, bson.M{"_id": ucp.CompanyID})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	payMethod, income := []map[string]interface{}{}, []map[string]interface{}{}
	for _, p := range c.PayMethod {
		payMethod = append(payMethod, map[string]interface{}{
			"name":    p.Name,
			"display": p.Display,
		})
	}

	for _, i := range c.Incomes {
		income = append(income, map[string]interface{}{
			"name":    i.Name,
			"display": i.Display,
		})
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"token":      t,
		"permission": ucp.Permission,
		"payMethod":  payMethod,
		"incomes":    income,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
